//https://code.google.com/codejam/contest/975485/dashboard
//instead of thinking fantasies, just run as a simple simulation and result is pretty fast to move on.
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;


int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    int T, N, target;

    char robo;
    fin >> T;
    
    for (int c=0; c<T; ++c) {
        vector<vector<int> > robovec(2);
        vector<char> turn;
        int bpos=0, opos=0, count=0;
        int bplace=1, oplace=1;
        int turnpos=0;
        fin >> N;
        while(N--) {
            fin >> robo >> target;
            if(robo=='B')robovec[0].push_back(target);
            else robovec[1].push_back(target);
            turn.push_back(robo);
        }
        bool justpressed;
        while(bpos<robovec[0].size() || opos<robovec[1].size()) {
            justpressed = false;
            count ++;
            if(bpos<robovec[0].size() && robovec[0][bpos]!=bplace) {
                bplace += (robovec[0][bpos]-bplace)/abs(robovec[0][bpos]-bplace);
            } else if(bpos<robovec[0].size() && robovec[0][bpos]==bplace && turn[turnpos] == 'B') {
                bpos++;
                turnpos++;
                justpressed = true;
            }
            if(opos<robovec[1].size() && robovec[1][opos]!=oplace) {
                oplace += (robovec[1][opos]-oplace)/abs(robovec[1][opos]-oplace);
            } else if(opos<robovec[1].size() && robovec[1][opos]==oplace && turn[turnpos] == 'O' && !justpressed) {
                opos++;
                turnpos++;
            }
            
            
        }
        
        fout << "Case #" << c+1 << ": " << count << endl;
        cout << "Case #" << c+1 << ": " << count << endl;
    }
    fin.close();
    fout.close();
}

