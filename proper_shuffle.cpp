//https://code.google.com/codejam/contest/2984486/dashboard#s=p2&a=2
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <algorithm>
using namespace std;

fstream fout;

void print_arr(int arr[], int len) {
    for(int i =0; i<len; ++i) {
        cout << arr[i] << " ";
    }
    cout << endl;
}


/*
void clear_histvec(vector<map<int, int> *> &histvec) {
    for (int i=0; i<histvec.size(); ++i) {
        delete histvec[i];
    }
    histvec.clear();
}

void prepare_histvec(vector<map<int, int> *> &histvec, int len ) {
    clear_histvec(histvec);
    map<int, int> *tmp;
    for (int i=0; i<len; ++i) {
        tmp = new map<int, int>;
        histvec.push_back(tmp);
    }
}
 

void make_hist(int arr[], int len, vector <map<int, int> *> &histvec) {
    for (int i=0; i<histvec.size(); ++i) {
        map<int, int> &tmp = (*(histvec[i]));
        tmp[arr[i]]++;
    }
}

void print_hist(vector <map<int,int> *>&histvec) {
    int count = 0;
    for (map<int, int> *hist:histvec) {
        count = 0;
        for (map<int,int>::iterator it = hist->begin(); it != hist->end(); ++it) {
            //cout << it->first << " " << it->second << endl;
            cout << it->second << ", ";
            fout << it->second << ", ";
            count += it->second;
            
        }
        cout << endl;
        fout << endl;
    }
}
*/


void good_algorithm(int arr[], int len) {
    int p=0;
    for (int i=0; i<len; ++i) {
        p = i+(rand()%(len-i));
        int tmp = arr[i];
        arr[i] = arr[p];
        arr[p] = tmp;
    }
}

void bad_algorithm(int arr[], int len) {
    int p=0;
    for (int i=0; i<len; ++i) {
        p = (rand()%(len));
        int tmp = arr[i];
        arr[i] = arr[p];
        arr[p] = tmp;
    }
}

void prepare_arr(int arr[], int len) {
    for(int i=0; i<len; ++i) {
        arr[i] = i;
    }
}

int score(int arr[], int len) {
    int sc = 0;
    for (int i=0; i<len; ++i){
        if(arr[i]<=i)
            sc++;
    }
    return sc;
}

const int LEN = 10;

/*
int main() {
    fstream fin;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    int T, N;
    int arr[LEN];
    fin >> T;
    for (int c=0; c<T; ++c)
    {
        memset(arr, 0, sizeof(int)*LEN);
        fin >> N;
        for(int i=0; i<N; ++i) {
            fin >> arr[i];
        }
        
        int s = score(arr,LEN);
        if(s < (472+500)/2) {
            fout << "Case #" << c+1 << ": " << "BAD" << endl;
            cout << "Case #" << c+1 << ": " << "BAD" << endl;
        } else {
            fout << "Case #" << c+1 << ": " << "GOOD" << endl;
            cout << "Case #" << c+1 << ": " << "GOOD" << endl;
        }
    }
    
    fout.close();
}*/


int main(){
    fout.open("output1.csv", ios::out);
    if(!fout.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    //vector <map<int, int> *> hist;
    map<int, int> hist2;
    srand((unsigned int)time(NULL));
    int arr[LEN];
    prepare_arr(arr,LEN);
    //prepare_histvec(hist, LEN);
    print_arr(arr, LEN);
    int sc=0;
    for(int i=0; i<10000; ++i) {
        good_algorithm(arr, LEN);
        //fout << score(arr, LEN) << endl;
        sc = score(arr, LEN);
        hist2[sc]++;
        //make_hist(arr, LEN, hist);
    }
    print_arr(arr, LEN);
    
    unsigned currentMax = 0;
    unsigned arg_max = 0;
    for(auto it = hist2.begin(); it != hist2.end(); ++it ) {
        if (it ->second > currentMax) {
            arg_max = it->first;
            currentMax = it->second;
            
        }
        cout << it->first << " - " << it->second << endl;
    }
    
    cout << arg_max << endl;
    //print_arr(arr, LEN);
    //print_hist(hist);
    
    cout << endl;
    cout << endl;
    cout << endl;
    
    prepare_arr(arr, LEN);
    //prepare_histvec(hist, LEN);
    sc = 0;
    print_arr(arr, LEN);
    hist2.clear();
    for(int i=0; i<10000; ++i) {
        bad_algorithm(arr, LEN);
        sc = score(arr, LEN);
        hist2[sc]++;
        //make_hist(arr, LEN, hist);
    }
    print_arr(arr, LEN);
    currentMax = 0;
    arg_max = 0;
    for(auto it = hist2.begin(); it != hist2.end(); ++it ) {
        if (it ->second > currentMax) {
            arg_max = it->first;
            currentMax = it->second;
            
        }
        cout << it->first << " - " << it->second << endl;
    }
    cout << arg_max << endl;
    //print_arr(arr, LEN);
    //print_hist(hist);
    
    //clear_histvec(hist);

    fout.close();
}