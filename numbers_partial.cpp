//https://code.google.com/codejam/contest/32016/dashboard#s=p2&a=2
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
#include <gmp.h>
#include <gmpxx.h>
using namespace std;



/*
 double a =pow(2.0001, 2);
 double b =pow(2.0, 5);
 double d =pow(9.0, 5);
 cout << fixed << setprecision(10)<<a << endl;
 cout << fixed << setprecision(10)<<b << endl;
 cout << fixed << setprecision(10)<<d << endl;
 */

mpz_class pow_x ( mpz_class x , unsigned i )
{
    mpz_class prod=1;
    if ( i == 0 )
        return 1;
    while ( i )
    {
        prod*=x;
        i--;
    }
    return prod;
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    int T;
    unsigned int N;
    fin >> T;
    
//    for (int c=1; c<300; ++c) {
    for (int c=0; c<T; ++c) {
        fin >> N;
        //N = c;x
        mpz_class base = (3 + sqrt(Nm));
        auto res1 =  pow_x(base, N);
        cout << res1.get_str() << endl;
        //cout << fixed << setprecision(1000) << base << endl;

        //cout << fixed << setprecision(1) << res1 << endl;
        stringstream ss;
        ss <<fixed << setprecision(1000) << setfill('0')<< setw(1005)<< res1;
        auto found = ss.str().find_last_of(".");
        string port = ss.str().substr(found-3, 3);
        fout << "Case #" << c+1 << ": " << port << endl;
        cout << "Case #" << c+1 << ": " << port << endl;
    }
    fin.close();
    fout.close();
}

