//https://code.google.com/codejam/contest/90101/dashboard#s=p0&a=0
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
#include <tr1/unordered_map>
using namespace std;

size_t get_remaining_dict_size(const string & str, map <string, bool> dictmap) {
    //map <string, bool> tmpmap = dictmap;
    
    //for(map<string, bool>::iterator it = dictmap.begin(); it != dictmap.end(); ++it) {cout << it->first << " " << it->second << endl;};
    //cout << endl;
    for(map<string, bool>::iterator it = dictmap.begin(); it != dictmap.end(); ) {
        bool deleted = false;
        //cout << it->first << endl;
        if(strcmp(it->first.c_str(),("nwlrbbmqbh")) == 0) {
        //    cout <<"found" << endl;
        }
        int charpos = -1;
        bool inbrackets = false;
        bool previous_close = false;
        bool found_previously = false;
        bool hit_previous = false;
        for(const auto &s: str) {
            if(s=='(') {
                if(!previous_close && !hit_previous) {
                    charpos++;
                    found_previously = false;
                }
                hit_previous = false;
                inbrackets = true;
                previous_close = false;
            } else if(s==')') {
                inbrackets = false;
                charpos++;
                found_previously = false;
                hit_previous = false;
                previous_close = true;
                if(!it->second) {
                    dictmap.erase(it++);
                    deleted = true;
                    break;
                }
            } else {
                if (charpos == -1) {
                    charpos++;
                }
                
                if (it->first[charpos] != s && !found_previously) {
                    it->second = false;
                } else {
                    found_previously = true;
                    it->second = true;
                }
                
                if(!inbrackets) {
                    charpos++;
                    found_previously = false;
                    hit_previous = true;
                    if(!it->second) {
                        dictmap.erase(it++);
                        deleted = true;
                        break;
                    }
                }
                previous_close = false;
            }
        }
        if(!deleted)it++;
    }
    //for(map<string, bool>::iterator it = dictmap.begin(); it != dictmap.end(); ++it) {cout << it->first << " " << it->second << endl;};
    
    return dictmap.size();
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    //    tr1::unordered_map<string, bool> dictmap;
    map<string, bool> dictmap;
    
    unsigned int N, D, L;
    fin >> L >> D >> N;
    for(int d=0; d<D; ++d) {
        string str;
        fin>>str;
        dictmap[str] = true;
    }
    
    for (int c=1; c<=N; ++c) {
        string str;
        fin >> str;
        size_t t = get_remaining_dict_size(str, dictmap);
        fout << "Case #" << c << ": " <<  t << endl;
        cout << "Case #" << c << ": " << t << endl;
    }
    fin.close();
    fout.close();
}
