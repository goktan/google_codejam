//https://code.google.com/codejam/contest/433101/dashboard
//Better solution is at snapper_chain.cpp, here I did think about input as well but this was 
//unnecessary. instead I should just check if all the snappers on certain snap are 1 or not!
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;

int get_state(unsigned long int snapper, unsigned long int snaps) {
    int mask = 1<<(snapper-1);
    return ((snaps & mask) > 0)?1:0;
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    int T;
    unsigned long int N, K;
    fin >> T;

    for (int c=0; c<T; ++c) {
        fin >> N >> K;
        // there is a pattern here, 1,3,1,7,1,3,1,15,1,3,1,7,1,3,1 and it turns out that this pattern is
        // for each snap, the input goes to snap ^ snap+1 from snap 1 to snap N
        // and for each snap, state goes to snaps binary value from snap 1 to snap N
        // for pattern, see below comments after main
        unsigned long int input_bin = (K xor (K+1));
        int input = get_state(N, input_bin);
        int status = get_state(N, K);
        int output = input & status;
        fout << "Case #" << c+1 << ": " << (output==1?"ON":"OFF") << endl;
        cout << "Case #" << c+1 << ": " << (output==1?"ON":"OFF") << endl;
    }
    fin.close();
    fout.close();
}

/* Following code works fine for small set but it is very slow on large since it each time calculates all 
 
 #include <iostream>
 #include <cmath>
 #include <fstream>
 #include <vector>
 #include <stack>
 #include <sstream>
 #include <map>
 #include <iomanip>
 #include <algorithm>
 #include <stdint.h>
 using namespace std;
 
 typedef struct Snapper {
 int input = 0;
 int output = 0;
 int state = 0;
 }Snapper;
 
 void print_snapper(const Snapper& sn) {
 cout << "in: " << sn.input << " - st: " << sn.state <<" - out: " << sn.output << endl;
 }
 
 int main() {
 fstream fin;
 fstream fout;
 fout.open("output.txt", ios::out);
 fin.open("input.txt", ios::in);
 if(!fout.is_open() || !fin.is_open()) {
 cerr << "file not opened" << endl;
 return EXIT_FAILURE;
 }
 int T;
 unsigned long int N, K;
 fin >> T;
 
 for (int c=0; c<T; ++c) {
 fin >> N >> K;
 vector<Snapper> snappers(N+1);
 snappers[0].output = 1; //base plug, dummy snap
 snappers[1].input = 1; //base plug, dummy snap
 cout << "snap: " << 0 << " snapper " << 1 << " " ; print_snapper(snappers[1]); cout << endl;
 for(int i=1; i<=K; ++i) {
 for(int j=1; j<=N; ++j) {
 if(snappers[j].input==1) {
 snappers[j].state xor_eq 1;
 }
 snappers[j].input = snappers[j-1].output;
 snappers[j].output = snappers[j].input and snappers[j].state;
 cout << "snap: " << i << " snapper " << j << " " ; print_snapper(snappers[j]);
 }
 cout << endl;
 
 }
 
 fout << "Case #" << c+1 << ": " << (snappers[N].output==1?"ON":"OFF") << endl;
 cout << "Case #" << c+1 << ": " << (snappers[N].output==1?"ON":"OFF") << endl;
 }
 fin.close();
 fout.close();
 }
 
*/

// see following for the patter definition http://arxiv.org/pdf/1308.0066.pdf

/*snap: 0 snapper 1 in: 1 - st: 0 - out: 0
 snap: 0 snapper 2 in: 0 - st: 0 - out: 0
 snap: 0 snapper 3 in: 0 - st: 0 - out: 0
 snap: 0 snapper 4 in: 0 - st: 0 - out: 0
 snap: 0 snapper 5 in: 0 - st: 0 - out: 0
 snap: 0 snapper 6 in: 0 - st: 0 - out: 0
 snap: 0 snapper 7 in: 0 - st: 0 - out: 0
 snap: 0 snapper 8 in: 0 - st: 0 - out: 0
 snap: 0 snapper 9 in: 0 - st: 0 - out: 0
 snap: 0 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 1 snapper 1 in: 1 - st: 1 - out: 1
 snap: 1 snapper 2 in: 1 - st: 0 - out: 0
 snap: 1 snapper 3 in: 0 - st: 0 - out: 0
 snap: 1 snapper 4 in: 0 - st: 0 - out: 0
 snap: 1 snapper 5 in: 0 - st: 0 - out: 0
 snap: 1 snapper 6 in: 0 - st: 0 - out: 0
 snap: 1 snapper 7 in: 0 - st: 0 - out: 0
 snap: 1 snapper 8 in: 0 - st: 0 - out: 0
 snap: 1 snapper 9 in: 0 - st: 0 - out: 0
 snap: 1 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 2 snapper 1 in: 1 - st: 0 - out: 0
 snap: 2 snapper 2 in: 0 - st: 1 - out: 0
 snap: 2 snapper 3 in: 0 - st: 0 - out: 0
 snap: 2 snapper 4 in: 0 - st: 0 - out: 0
 snap: 2 snapper 5 in: 0 - st: 0 - out: 0
 snap: 2 snapper 6 in: 0 - st: 0 - out: 0
 snap: 2 snapper 7 in: 0 - st: 0 - out: 0
 snap: 2 snapper 8 in: 0 - st: 0 - out: 0
 snap: 2 snapper 9 in: 0 - st: 0 - out: 0
 snap: 2 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 3 snapper 1 in: 1 - st: 1 - out: 1
 snap: 3 snapper 2 in: 1 - st: 1 - out: 1
 snap: 3 snapper 3 in: 1 - st: 0 - out: 0
 snap: 3 snapper 4 in: 0 - st: 0 - out: 0
 snap: 3 snapper 5 in: 0 - st: 0 - out: 0
 snap: 3 snapper 6 in: 0 - st: 0 - out: 0
 snap: 3 snapper 7 in: 0 - st: 0 - out: 0
 snap: 3 snapper 8 in: 0 - st: 0 - out: 0
 snap: 3 snapper 9 in: 0 - st: 0 - out: 0
 snap: 3 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 4 snapper 1 in: 1 - st: 0 - out: 0
 snap: 4 snapper 2 in: 0 - st: 0 - out: 0
 snap: 4 snapper 3 in: 0 - st: 1 - out: 0
 snap: 4 snapper 4 in: 0 - st: 0 - out: 0
 snap: 4 snapper 5 in: 0 - st: 0 - out: 0
 snap: 4 snapper 6 in: 0 - st: 0 - out: 0
 snap: 4 snapper 7 in: 0 - st: 0 - out: 0
 snap: 4 snapper 8 in: 0 - st: 0 - out: 0
 snap: 4 snapper 9 in: 0 - st: 0 - out: 0
 snap: 4 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 5 snapper 1 in: 1 - st: 1 - out: 1
 snap: 5 snapper 2 in: 1 - st: 0 - out: 0
 snap: 5 snapper 3 in: 0 - st: 1 - out: 0
 snap: 5 snapper 4 in: 0 - st: 0 - out: 0
 snap: 5 snapper 5 in: 0 - st: 0 - out: 0
 snap: 5 snapper 6 in: 0 - st: 0 - out: 0
 snap: 5 snapper 7 in: 0 - st: 0 - out: 0
 snap: 5 snapper 8 in: 0 - st: 0 - out: 0
 snap: 5 snapper 9 in: 0 - st: 0 - out: 0
 snap: 5 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 6 snapper 1 in: 1 - st: 0 - out: 0
 snap: 6 snapper 2 in: 0 - st: 1 - out: 0
 snap: 6 snapper 3 in: 0 - st: 1 - out: 0
 snap: 6 snapper 4 in: 0 - st: 0 - out: 0
 snap: 6 snapper 5 in: 0 - st: 0 - out: 0
 snap: 6 snapper 6 in: 0 - st: 0 - out: 0
 snap: 6 snapper 7 in: 0 - st: 0 - out: 0
 snap: 6 snapper 8 in: 0 - st: 0 - out: 0
 snap: 6 snapper 9 in: 0 - st: 0 - out: 0
 snap: 6 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 7 snapper 1 in: 1 - st: 1 - out: 1
 snap: 7 snapper 2 in: 1 - st: 1 - out: 1
 snap: 7 snapper 3 in: 1 - st: 1 - out: 1
 snap: 7 snapper 4 in: 1 - st: 0 - out: 0
 snap: 7 snapper 5 in: 0 - st: 0 - out: 0
 snap: 7 snapper 6 in: 0 - st: 0 - out: 0
 snap: 7 snapper 7 in: 0 - st: 0 - out: 0
 snap: 7 snapper 8 in: 0 - st: 0 - out: 0
 snap: 7 snapper 9 in: 0 - st: 0 - out: 0
 snap: 7 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 8 snapper 1 in: 1 - st: 0 - out: 0
 snap: 8 snapper 2 in: 0 - st: 0 - out: 0
 snap: 8 snapper 3 in: 0 - st: 0 - out: 0
 snap: 8 snapper 4 in: 0 - st: 1 - out: 0
 snap: 8 snapper 5 in: 0 - st: 0 - out: 0
 snap: 8 snapper 6 in: 0 - st: 0 - out: 0
 snap: 8 snapper 7 in: 0 - st: 0 - out: 0
 snap: 8 snapper 8 in: 0 - st: 0 - out: 0
 snap: 8 snapper 9 in: 0 - st: 0 - out: 0
 snap: 8 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 9 snapper 1 in: 1 - st: 1 - out: 1
 snap: 9 snapper 2 in: 1 - st: 0 - out: 0
 snap: 9 snapper 3 in: 0 - st: 0 - out: 0
 snap: 9 snapper 4 in: 0 - st: 1 - out: 0
 snap: 9 snapper 5 in: 0 - st: 0 - out: 0
 snap: 9 snapper 6 in: 0 - st: 0 - out: 0
 snap: 9 snapper 7 in: 0 - st: 0 - out: 0
 snap: 9 snapper 8 in: 0 - st: 0 - out: 0
 snap: 9 snapper 9 in: 0 - st: 0 - out: 0
 snap: 9 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 10 snapper 1 in: 1 - st: 0 - out: 0
 snap: 10 snapper 2 in: 0 - st: 1 - out: 0
 snap: 10 snapper 3 in: 0 - st: 0 - out: 0
 snap: 10 snapper 4 in: 0 - st: 1 - out: 0
 snap: 10 snapper 5 in: 0 - st: 0 - out: 0
 snap: 10 snapper 6 in: 0 - st: 0 - out: 0
 snap: 10 snapper 7 in: 0 - st: 0 - out: 0
 snap: 10 snapper 8 in: 0 - st: 0 - out: 0
 snap: 10 snapper 9 in: 0 - st: 0 - out: 0
 snap: 10 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 11 snapper 1 in: 1 - st: 1 - out: 1
 snap: 11 snapper 2 in: 1 - st: 1 - out: 1
 snap: 11 snapper 3 in: 1 - st: 0 - out: 0
 snap: 11 snapper 4 in: 0 - st: 1 - out: 0
 snap: 11 snapper 5 in: 0 - st: 0 - out: 0
 snap: 11 snapper 6 in: 0 - st: 0 - out: 0
 snap: 11 snapper 7 in: 0 - st: 0 - out: 0
 snap: 11 snapper 8 in: 0 - st: 0 - out: 0
 snap: 11 snapper 9 in: 0 - st: 0 - out: 0
 snap: 11 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 12 snapper 1 in: 1 - st: 0 - out: 0
 snap: 12 snapper 2 in: 0 - st: 0 - out: 0
 snap: 12 snapper 3 in: 0 - st: 1 - out: 0
 snap: 12 snapper 4 in: 0 - st: 1 - out: 0
 snap: 12 snapper 5 in: 0 - st: 0 - out: 0
 snap: 12 snapper 6 in: 0 - st: 0 - out: 0
 snap: 12 snapper 7 in: 0 - st: 0 - out: 0
 snap: 12 snapper 8 in: 0 - st: 0 - out: 0
 snap: 12 snapper 9 in: 0 - st: 0 - out: 0
 snap: 12 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 13 snapper 1 in: 1 - st: 1 - out: 1
 snap: 13 snapper 2 in: 1 - st: 0 - out: 0
 snap: 13 snapper 3 in: 0 - st: 1 - out: 0
 snap: 13 snapper 4 in: 0 - st: 1 - out: 0
 snap: 13 snapper 5 in: 0 - st: 0 - out: 0
 snap: 13 snapper 6 in: 0 - st: 0 - out: 0
 snap: 13 snapper 7 in: 0 - st: 0 - out: 0
 snap: 13 snapper 8 in: 0 - st: 0 - out: 0
 snap: 13 snapper 9 in: 0 - st: 0 - out: 0
 snap: 13 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 14 snapper 1 in: 1 - st: 0 - out: 0
 snap: 14 snapper 2 in: 0 - st: 1 - out: 0
 snap: 14 snapper 3 in: 0 - st: 1 - out: 0
 snap: 14 snapper 4 in: 0 - st: 1 - out: 0
 snap: 14 snapper 5 in: 0 - st: 0 - out: 0
 snap: 14 snapper 6 in: 0 - st: 0 - out: 0
 snap: 14 snapper 7 in: 0 - st: 0 - out: 0
 snap: 14 snapper 8 in: 0 - st: 0 - out: 0
 snap: 14 snapper 9 in: 0 - st: 0 - out: 0
 snap: 14 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 15 snapper 1 in: 1 - st: 1 - out: 1
 snap: 15 snapper 2 in: 1 - st: 1 - out: 1
 snap: 15 snapper 3 in: 1 - st: 1 - out: 1
 snap: 15 snapper 4 in: 1 - st: 1 - out: 1
 snap: 15 snapper 5 in: 1 - st: 0 - out: 0
 snap: 15 snapper 6 in: 0 - st: 0 - out: 0
 snap: 15 snapper 7 in: 0 - st: 0 - out: 0
 snap: 15 snapper 8 in: 0 - st: 0 - out: 0
 snap: 15 snapper 9 in: 0 - st: 0 - out: 0
 snap: 15 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 16 snapper 1 in: 1 - st: 0 - out: 0
 snap: 16 snapper 2 in: 0 - st: 0 - out: 0
 snap: 16 snapper 3 in: 0 - st: 0 - out: 0
 snap: 16 snapper 4 in: 0 - st: 0 - out: 0
 snap: 16 snapper 5 in: 0 - st: 1 - out: 0
 snap: 16 snapper 6 in: 0 - st: 0 - out: 0
 snap: 16 snapper 7 in: 0 - st: 0 - out: 0
 snap: 16 snapper 8 in: 0 - st: 0 - out: 0
 snap: 16 snapper 9 in: 0 - st: 0 - out: 0
 snap: 16 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 17 snapper 1 in: 1 - st: 1 - out: 1
 snap: 17 snapper 2 in: 1 - st: 0 - out: 0
 snap: 17 snapper 3 in: 0 - st: 0 - out: 0
 snap: 17 snapper 4 in: 0 - st: 0 - out: 0
 snap: 17 snapper 5 in: 0 - st: 1 - out: 0
 snap: 17 snapper 6 in: 0 - st: 0 - out: 0
 snap: 17 snapper 7 in: 0 - st: 0 - out: 0
 snap: 17 snapper 8 in: 0 - st: 0 - out: 0
 snap: 17 snapper 9 in: 0 - st: 0 - out: 0
 snap: 17 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 18 snapper 1 in: 1 - st: 0 - out: 0
 snap: 18 snapper 2 in: 0 - st: 1 - out: 0
 snap: 18 snapper 3 in: 0 - st: 0 - out: 0
 snap: 18 snapper 4 in: 0 - st: 0 - out: 0
 snap: 18 snapper 5 in: 0 - st: 1 - out: 0
 snap: 18 snapper 6 in: 0 - st: 0 - out: 0
 snap: 18 snapper 7 in: 0 - st: 0 - out: 0
 snap: 18 snapper 8 in: 0 - st: 0 - out: 0
 snap: 18 snapper 9 in: 0 - st: 0 - out: 0
 snap: 18 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 19 snapper 1 in: 1 - st: 1 - out: 1
 snap: 19 snapper 2 in: 1 - st: 1 - out: 1
 snap: 19 snapper 3 in: 1 - st: 0 - out: 0
 snap: 19 snapper 4 in: 0 - st: 0 - out: 0
 snap: 19 snapper 5 in: 0 - st: 1 - out: 0
 snap: 19 snapper 6 in: 0 - st: 0 - out: 0
 snap: 19 snapper 7 in: 0 - st: 0 - out: 0
 snap: 19 snapper 8 in: 0 - st: 0 - out: 0
 snap: 19 snapper 9 in: 0 - st: 0 - out: 0
 snap: 19 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 20 snapper 1 in: 1 - st: 0 - out: 0
 snap: 20 snapper 2 in: 0 - st: 0 - out: 0
 snap: 20 snapper 3 in: 0 - st: 1 - out: 0
 snap: 20 snapper 4 in: 0 - st: 0 - out: 0
 snap: 20 snapper 5 in: 0 - st: 1 - out: 0
 snap: 20 snapper 6 in: 0 - st: 0 - out: 0
 snap: 20 snapper 7 in: 0 - st: 0 - out: 0
 snap: 20 snapper 8 in: 0 - st: 0 - out: 0
 snap: 20 snapper 9 in: 0 - st: 0 - out: 0
 snap: 20 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 21 snapper 1 in: 1 - st: 1 - out: 1
 snap: 21 snapper 2 in: 1 - st: 0 - out: 0
 snap: 21 snapper 3 in: 0 - st: 1 - out: 0
 snap: 21 snapper 4 in: 0 - st: 0 - out: 0
 snap: 21 snapper 5 in: 0 - st: 1 - out: 0
 snap: 21 snapper 6 in: 0 - st: 0 - out: 0
 snap: 21 snapper 7 in: 0 - st: 0 - out: 0
 snap: 21 snapper 8 in: 0 - st: 0 - out: 0
 snap: 21 snapper 9 in: 0 - st: 0 - out: 0
 snap: 21 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 22 snapper 1 in: 1 - st: 0 - out: 0
 snap: 22 snapper 2 in: 0 - st: 1 - out: 0
 snap: 22 snapper 3 in: 0 - st: 1 - out: 0
 snap: 22 snapper 4 in: 0 - st: 0 - out: 0
 snap: 22 snapper 5 in: 0 - st: 1 - out: 0
 snap: 22 snapper 6 in: 0 - st: 0 - out: 0
 snap: 22 snapper 7 in: 0 - st: 0 - out: 0
 snap: 22 snapper 8 in: 0 - st: 0 - out: 0
 snap: 22 snapper 9 in: 0 - st: 0 - out: 0
 snap: 22 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 23 snapper 1 in: 1 - st: 1 - out: 1
 snap: 23 snapper 2 in: 1 - st: 1 - out: 1
 snap: 23 snapper 3 in: 1 - st: 1 - out: 1
 snap: 23 snapper 4 in: 1 - st: 0 - out: 0
 snap: 23 snapper 5 in: 0 - st: 1 - out: 0
 snap: 23 snapper 6 in: 0 - st: 0 - out: 0
 snap: 23 snapper 7 in: 0 - st: 0 - out: 0
 snap: 23 snapper 8 in: 0 - st: 0 - out: 0
 snap: 23 snapper 9 in: 0 - st: 0 - out: 0
 snap: 23 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 24 snapper 1 in: 1 - st: 0 - out: 0
 snap: 24 snapper 2 in: 0 - st: 0 - out: 0
 snap: 24 snapper 3 in: 0 - st: 0 - out: 0
 snap: 24 snapper 4 in: 0 - st: 1 - out: 0
 snap: 24 snapper 5 in: 0 - st: 1 - out: 0
 snap: 24 snapper 6 in: 0 - st: 0 - out: 0
 snap: 24 snapper 7 in: 0 - st: 0 - out: 0
 snap: 24 snapper 8 in: 0 - st: 0 - out: 0
 snap: 24 snapper 9 in: 0 - st: 0 - out: 0
 snap: 24 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 25 snapper 1 in: 1 - st: 1 - out: 1
 snap: 25 snapper 2 in: 1 - st: 0 - out: 0
 snap: 25 snapper 3 in: 0 - st: 0 - out: 0
 snap: 25 snapper 4 in: 0 - st: 1 - out: 0
 snap: 25 snapper 5 in: 0 - st: 1 - out: 0
 snap: 25 snapper 6 in: 0 - st: 0 - out: 0
 snap: 25 snapper 7 in: 0 - st: 0 - out: 0
 snap: 25 snapper 8 in: 0 - st: 0 - out: 0
 snap: 25 snapper 9 in: 0 - st: 0 - out: 0
 snap: 25 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 26 snapper 1 in: 1 - st: 0 - out: 0
 snap: 26 snapper 2 in: 0 - st: 1 - out: 0
 snap: 26 snapper 3 in: 0 - st: 0 - out: 0
 snap: 26 snapper 4 in: 0 - st: 1 - out: 0
 snap: 26 snapper 5 in: 0 - st: 1 - out: 0
 snap: 26 snapper 6 in: 0 - st: 0 - out: 0
 snap: 26 snapper 7 in: 0 - st: 0 - out: 0
 snap: 26 snapper 8 in: 0 - st: 0 - out: 0
 snap: 26 snapper 9 in: 0 - st: 0 - out: 0
 snap: 26 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 27 snapper 1 in: 1 - st: 1 - out: 1
 snap: 27 snapper 2 in: 1 - st: 1 - out: 1
 snap: 27 snapper 3 in: 1 - st: 0 - out: 0
 snap: 27 snapper 4 in: 0 - st: 1 - out: 0
 snap: 27 snapper 5 in: 0 - st: 1 - out: 0
 snap: 27 snapper 6 in: 0 - st: 0 - out: 0
 snap: 27 snapper 7 in: 0 - st: 0 - out: 0
 snap: 27 snapper 8 in: 0 - st: 0 - out: 0
 snap: 27 snapper 9 in: 0 - st: 0 - out: 0
 snap: 27 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 28 snapper 1 in: 1 - st: 0 - out: 0
 snap: 28 snapper 2 in: 0 - st: 0 - out: 0
 snap: 28 snapper 3 in: 0 - st: 1 - out: 0
 snap: 28 snapper 4 in: 0 - st: 1 - out: 0
 snap: 28 snapper 5 in: 0 - st: 1 - out: 0
 snap: 28 snapper 6 in: 0 - st: 0 - out: 0
 snap: 28 snapper 7 in: 0 - st: 0 - out: 0
 snap: 28 snapper 8 in: 0 - st: 0 - out: 0
 snap: 28 snapper 9 in: 0 - st: 0 - out: 0
 snap: 28 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 29 snapper 1 in: 1 - st: 1 - out: 1
 snap: 29 snapper 2 in: 1 - st: 0 - out: 0
 snap: 29 snapper 3 in: 0 - st: 1 - out: 0
 snap: 29 snapper 4 in: 0 - st: 1 - out: 0
 snap: 29 snapper 5 in: 0 - st: 1 - out: 0
 snap: 29 snapper 6 in: 0 - st: 0 - out: 0
 snap: 29 snapper 7 in: 0 - st: 0 - out: 0
 snap: 29 snapper 8 in: 0 - st: 0 - out: 0
 snap: 29 snapper 9 in: 0 - st: 0 - out: 0
 snap: 29 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 30 snapper 1 in: 1 - st: 0 - out: 0
 snap: 30 snapper 2 in: 0 - st: 1 - out: 0
 snap: 30 snapper 3 in: 0 - st: 1 - out: 0
 snap: 30 snapper 4 in: 0 - st: 1 - out: 0
 snap: 30 snapper 5 in: 0 - st: 1 - out: 0
 snap: 30 snapper 6 in: 0 - st: 0 - out: 0
 snap: 30 snapper 7 in: 0 - st: 0 - out: 0
 snap: 30 snapper 8 in: 0 - st: 0 - out: 0
 snap: 30 snapper 9 in: 0 - st: 0 - out: 0
 snap: 30 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 31 snapper 1 in: 1 - st: 1 - out: 1
 snap: 31 snapper 2 in: 1 - st: 1 - out: 1
 snap: 31 snapper 3 in: 1 - st: 1 - out: 1
 snap: 31 snapper 4 in: 1 - st: 1 - out: 1
 snap: 31 snapper 5 in: 1 - st: 1 - out: 1
 snap: 31 snapper 6 in: 1 - st: 0 - out: 0
 snap: 31 snapper 7 in: 0 - st: 0 - out: 0
 snap: 31 snapper 8 in: 0 - st: 0 - out: 0
 snap: 31 snapper 9 in: 0 - st: 0 - out: 0
 snap: 31 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 32 snapper 1 in: 1 - st: 0 - out: 0
 snap: 32 snapper 2 in: 0 - st: 0 - out: 0
 snap: 32 snapper 3 in: 0 - st: 0 - out: 0
 snap: 32 snapper 4 in: 0 - st: 0 - out: 0
 snap: 32 snapper 5 in: 0 - st: 0 - out: 0
 snap: 32 snapper 6 in: 0 - st: 1 - out: 0
 snap: 32 snapper 7 in: 0 - st: 0 - out: 0
 snap: 32 snapper 8 in: 0 - st: 0 - out: 0
 snap: 32 snapper 9 in: 0 - st: 0 - out: 0
 snap: 32 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 33 snapper 1 in: 1 - st: 1 - out: 1
 snap: 33 snapper 2 in: 1 - st: 0 - out: 0
 snap: 33 snapper 3 in: 0 - st: 0 - out: 0
 snap: 33 snapper 4 in: 0 - st: 0 - out: 0
 snap: 33 snapper 5 in: 0 - st: 0 - out: 0
 snap: 33 snapper 6 in: 0 - st: 1 - out: 0
 snap: 33 snapper 7 in: 0 - st: 0 - out: 0
 snap: 33 snapper 8 in: 0 - st: 0 - out: 0
 snap: 33 snapper 9 in: 0 - st: 0 - out: 0
 snap: 33 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 34 snapper 1 in: 1 - st: 0 - out: 0
 snap: 34 snapper 2 in: 0 - st: 1 - out: 0
 snap: 34 snapper 3 in: 0 - st: 0 - out: 0
 snap: 34 snapper 4 in: 0 - st: 0 - out: 0
 snap: 34 snapper 5 in: 0 - st: 0 - out: 0
 snap: 34 snapper 6 in: 0 - st: 1 - out: 0
 snap: 34 snapper 7 in: 0 - st: 0 - out: 0
 snap: 34 snapper 8 in: 0 - st: 0 - out: 0
 snap: 34 snapper 9 in: 0 - st: 0 - out: 0
 snap: 34 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 35 snapper 1 in: 1 - st: 1 - out: 1
 snap: 35 snapper 2 in: 1 - st: 1 - out: 1
 snap: 35 snapper 3 in: 1 - st: 0 - out: 0
 snap: 35 snapper 4 in: 0 - st: 0 - out: 0
 snap: 35 snapper 5 in: 0 - st: 0 - out: 0
 snap: 35 snapper 6 in: 0 - st: 1 - out: 0
 snap: 35 snapper 7 in: 0 - st: 0 - out: 0
 snap: 35 snapper 8 in: 0 - st: 0 - out: 0
 snap: 35 snapper 9 in: 0 - st: 0 - out: 0
 snap: 35 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 36 snapper 1 in: 1 - st: 0 - out: 0
 snap: 36 snapper 2 in: 0 - st: 0 - out: 0
 snap: 36 snapper 3 in: 0 - st: 1 - out: 0
 snap: 36 snapper 4 in: 0 - st: 0 - out: 0
 snap: 36 snapper 5 in: 0 - st: 0 - out: 0
 snap: 36 snapper 6 in: 0 - st: 1 - out: 0
 snap: 36 snapper 7 in: 0 - st: 0 - out: 0
 snap: 36 snapper 8 in: 0 - st: 0 - out: 0
 snap: 36 snapper 9 in: 0 - st: 0 - out: 0
 snap: 36 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 37 snapper 1 in: 1 - st: 1 - out: 1
 snap: 37 snapper 2 in: 1 - st: 0 - out: 0
 snap: 37 snapper 3 in: 0 - st: 1 - out: 0
 snap: 37 snapper 4 in: 0 - st: 0 - out: 0
 snap: 37 snapper 5 in: 0 - st: 0 - out: 0
 snap: 37 snapper 6 in: 0 - st: 1 - out: 0
 snap: 37 snapper 7 in: 0 - st: 0 - out: 0
 snap: 37 snapper 8 in: 0 - st: 0 - out: 0
 snap: 37 snapper 9 in: 0 - st: 0 - out: 0
 snap: 37 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 38 snapper 1 in: 1 - st: 0 - out: 0
 snap: 38 snapper 2 in: 0 - st: 1 - out: 0
 snap: 38 snapper 3 in: 0 - st: 1 - out: 0
 snap: 38 snapper 4 in: 0 - st: 0 - out: 0
 snap: 38 snapper 5 in: 0 - st: 0 - out: 0
 snap: 38 snapper 6 in: 0 - st: 1 - out: 0
 snap: 38 snapper 7 in: 0 - st: 0 - out: 0
 snap: 38 snapper 8 in: 0 - st: 0 - out: 0
 snap: 38 snapper 9 in: 0 - st: 0 - out: 0
 snap: 38 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 39 snapper 1 in: 1 - st: 1 - out: 1
 snap: 39 snapper 2 in: 1 - st: 1 - out: 1
 snap: 39 snapper 3 in: 1 - st: 1 - out: 1
 snap: 39 snapper 4 in: 1 - st: 0 - out: 0
 snap: 39 snapper 5 in: 0 - st: 0 - out: 0
 snap: 39 snapper 6 in: 0 - st: 1 - out: 0
 snap: 39 snapper 7 in: 0 - st: 0 - out: 0
 snap: 39 snapper 8 in: 0 - st: 0 - out: 0
 snap: 39 snapper 9 in: 0 - st: 0 - out: 0
 snap: 39 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 40 snapper 1 in: 1 - st: 0 - out: 0
 snap: 40 snapper 2 in: 0 - st: 0 - out: 0
 snap: 40 snapper 3 in: 0 - st: 0 - out: 0
 snap: 40 snapper 4 in: 0 - st: 1 - out: 0
 snap: 40 snapper 5 in: 0 - st: 0 - out: 0
 snap: 40 snapper 6 in: 0 - st: 1 - out: 0
 snap: 40 snapper 7 in: 0 - st: 0 - out: 0
 snap: 40 snapper 8 in: 0 - st: 0 - out: 0
 snap: 40 snapper 9 in: 0 - st: 0 - out: 0
 snap: 40 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 41 snapper 1 in: 1 - st: 1 - out: 1
 snap: 41 snapper 2 in: 1 - st: 0 - out: 0
 snap: 41 snapper 3 in: 0 - st: 0 - out: 0
 snap: 41 snapper 4 in: 0 - st: 1 - out: 0
 snap: 41 snapper 5 in: 0 - st: 0 - out: 0
 snap: 41 snapper 6 in: 0 - st: 1 - out: 0
 snap: 41 snapper 7 in: 0 - st: 0 - out: 0
 snap: 41 snapper 8 in: 0 - st: 0 - out: 0
 snap: 41 snapper 9 in: 0 - st: 0 - out: 0
 snap: 41 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 42 snapper 1 in: 1 - st: 0 - out: 0
 snap: 42 snapper 2 in: 0 - st: 1 - out: 0
 snap: 42 snapper 3 in: 0 - st: 0 - out: 0
 snap: 42 snapper 4 in: 0 - st: 1 - out: 0
 snap: 42 snapper 5 in: 0 - st: 0 - out: 0
 snap: 42 snapper 6 in: 0 - st: 1 - out: 0
 snap: 42 snapper 7 in: 0 - st: 0 - out: 0
 snap: 42 snapper 8 in: 0 - st: 0 - out: 0
 snap: 42 snapper 9 in: 0 - st: 0 - out: 0
 snap: 42 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 43 snapper 1 in: 1 - st: 1 - out: 1
 snap: 43 snapper 2 in: 1 - st: 1 - out: 1
 snap: 43 snapper 3 in: 1 - st: 0 - out: 0
 snap: 43 snapper 4 in: 0 - st: 1 - out: 0
 snap: 43 snapper 5 in: 0 - st: 0 - out: 0
 snap: 43 snapper 6 in: 0 - st: 1 - out: 0
 snap: 43 snapper 7 in: 0 - st: 0 - out: 0
 snap: 43 snapper 8 in: 0 - st: 0 - out: 0
 snap: 43 snapper 9 in: 0 - st: 0 - out: 0
 snap: 43 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 44 snapper 1 in: 1 - st: 0 - out: 0
 snap: 44 snapper 2 in: 0 - st: 0 - out: 0
 snap: 44 snapper 3 in: 0 - st: 1 - out: 0
 snap: 44 snapper 4 in: 0 - st: 1 - out: 0
 snap: 44 snapper 5 in: 0 - st: 0 - out: 0
 snap: 44 snapper 6 in: 0 - st: 1 - out: 0
 snap: 44 snapper 7 in: 0 - st: 0 - out: 0
 snap: 44 snapper 8 in: 0 - st: 0 - out: 0
 snap: 44 snapper 9 in: 0 - st: 0 - out: 0
 snap: 44 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 45 snapper 1 in: 1 - st: 1 - out: 1
 snap: 45 snapper 2 in: 1 - st: 0 - out: 0
 snap: 45 snapper 3 in: 0 - st: 1 - out: 0
 snap: 45 snapper 4 in: 0 - st: 1 - out: 0
 snap: 45 snapper 5 in: 0 - st: 0 - out: 0
 snap: 45 snapper 6 in: 0 - st: 1 - out: 0
 snap: 45 snapper 7 in: 0 - st: 0 - out: 0
 snap: 45 snapper 8 in: 0 - st: 0 - out: 0
 snap: 45 snapper 9 in: 0 - st: 0 - out: 0
 snap: 45 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 46 snapper 1 in: 1 - st: 0 - out: 0
 snap: 46 snapper 2 in: 0 - st: 1 - out: 0
 snap: 46 snapper 3 in: 0 - st: 1 - out: 0
 snap: 46 snapper 4 in: 0 - st: 1 - out: 0
 snap: 46 snapper 5 in: 0 - st: 0 - out: 0
 snap: 46 snapper 6 in: 0 - st: 1 - out: 0
 snap: 46 snapper 7 in: 0 - st: 0 - out: 0
 snap: 46 snapper 8 in: 0 - st: 0 - out: 0
 snap: 46 snapper 9 in: 0 - st: 0 - out: 0
 snap: 46 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 47 snapper 1 in: 1 - st: 1 - out: 1
 snap: 47 snapper 2 in: 1 - st: 1 - out: 1
 snap: 47 snapper 3 in: 1 - st: 1 - out: 1
 snap: 47 snapper 4 in: 1 - st: 1 - out: 1
 snap: 47 snapper 5 in: 1 - st: 0 - out: 0
 snap: 47 snapper 6 in: 0 - st: 1 - out: 0
 snap: 47 snapper 7 in: 0 - st: 0 - out: 0
 snap: 47 snapper 8 in: 0 - st: 0 - out: 0
 snap: 47 snapper 9 in: 0 - st: 0 - out: 0
 snap: 47 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 48 snapper 1 in: 1 - st: 0 - out: 0
 snap: 48 snapper 2 in: 0 - st: 0 - out: 0
 snap: 48 snapper 3 in: 0 - st: 0 - out: 0
 snap: 48 snapper 4 in: 0 - st: 0 - out: 0
 snap: 48 snapper 5 in: 0 - st: 1 - out: 0
 snap: 48 snapper 6 in: 0 - st: 1 - out: 0
 snap: 48 snapper 7 in: 0 - st: 0 - out: 0
 snap: 48 snapper 8 in: 0 - st: 0 - out: 0
 snap: 48 snapper 9 in: 0 - st: 0 - out: 0
 snap: 48 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 49 snapper 1 in: 1 - st: 1 - out: 1
 snap: 49 snapper 2 in: 1 - st: 0 - out: 0
 snap: 49 snapper 3 in: 0 - st: 0 - out: 0
 snap: 49 snapper 4 in: 0 - st: 0 - out: 0
 snap: 49 snapper 5 in: 0 - st: 1 - out: 0
 snap: 49 snapper 6 in: 0 - st: 1 - out: 0
 snap: 49 snapper 7 in: 0 - st: 0 - out: 0
 snap: 49 snapper 8 in: 0 - st: 0 - out: 0
 snap: 49 snapper 9 in: 0 - st: 0 - out: 0
 snap: 49 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 50 snapper 1 in: 1 - st: 0 - out: 0
 snap: 50 snapper 2 in: 0 - st: 1 - out: 0
 snap: 50 snapper 3 in: 0 - st: 0 - out: 0
 snap: 50 snapper 4 in: 0 - st: 0 - out: 0
 snap: 50 snapper 5 in: 0 - st: 1 - out: 0
 snap: 50 snapper 6 in: 0 - st: 1 - out: 0
 snap: 50 snapper 7 in: 0 - st: 0 - out: 0
 snap: 50 snapper 8 in: 0 - st: 0 - out: 0
 snap: 50 snapper 9 in: 0 - st: 0 - out: 0
 snap: 50 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 51 snapper 1 in: 1 - st: 1 - out: 1
 snap: 51 snapper 2 in: 1 - st: 1 - out: 1
 snap: 51 snapper 3 in: 1 - st: 0 - out: 0
 snap: 51 snapper 4 in: 0 - st: 0 - out: 0
 snap: 51 snapper 5 in: 0 - st: 1 - out: 0
 snap: 51 snapper 6 in: 0 - st: 1 - out: 0
 snap: 51 snapper 7 in: 0 - st: 0 - out: 0
 snap: 51 snapper 8 in: 0 - st: 0 - out: 0
 snap: 51 snapper 9 in: 0 - st: 0 - out: 0
 snap: 51 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 52 snapper 1 in: 1 - st: 0 - out: 0
 snap: 52 snapper 2 in: 0 - st: 0 - out: 0
 snap: 52 snapper 3 in: 0 - st: 1 - out: 0
 snap: 52 snapper 4 in: 0 - st: 0 - out: 0
 snap: 52 snapper 5 in: 0 - st: 1 - out: 0
 snap: 52 snapper 6 in: 0 - st: 1 - out: 0
 snap: 52 snapper 7 in: 0 - st: 0 - out: 0
 snap: 52 snapper 8 in: 0 - st: 0 - out: 0
 snap: 52 snapper 9 in: 0 - st: 0 - out: 0
 snap: 52 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 53 snapper 1 in: 1 - st: 1 - out: 1
 snap: 53 snapper 2 in: 1 - st: 0 - out: 0
 snap: 53 snapper 3 in: 0 - st: 1 - out: 0
 snap: 53 snapper 4 in: 0 - st: 0 - out: 0
 snap: 53 snapper 5 in: 0 - st: 1 - out: 0
 snap: 53 snapper 6 in: 0 - st: 1 - out: 0
 snap: 53 snapper 7 in: 0 - st: 0 - out: 0
 snap: 53 snapper 8 in: 0 - st: 0 - out: 0
 snap: 53 snapper 9 in: 0 - st: 0 - out: 0
 snap: 53 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 54 snapper 1 in: 1 - st: 0 - out: 0
 snap: 54 snapper 2 in: 0 - st: 1 - out: 0
 snap: 54 snapper 3 in: 0 - st: 1 - out: 0
 snap: 54 snapper 4 in: 0 - st: 0 - out: 0
 snap: 54 snapper 5 in: 0 - st: 1 - out: 0
 snap: 54 snapper 6 in: 0 - st: 1 - out: 0
 snap: 54 snapper 7 in: 0 - st: 0 - out: 0
 snap: 54 snapper 8 in: 0 - st: 0 - out: 0
 snap: 54 snapper 9 in: 0 - st: 0 - out: 0
 snap: 54 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 55 snapper 1 in: 1 - st: 1 - out: 1
 snap: 55 snapper 2 in: 1 - st: 1 - out: 1
 snap: 55 snapper 3 in: 1 - st: 1 - out: 1
 snap: 55 snapper 4 in: 1 - st: 0 - out: 0
 snap: 55 snapper 5 in: 0 - st: 1 - out: 0
 snap: 55 snapper 6 in: 0 - st: 1 - out: 0
 snap: 55 snapper 7 in: 0 - st: 0 - out: 0
 snap: 55 snapper 8 in: 0 - st: 0 - out: 0
 snap: 55 snapper 9 in: 0 - st: 0 - out: 0
 snap: 55 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 56 snapper 1 in: 1 - st: 0 - out: 0
 snap: 56 snapper 2 in: 0 - st: 0 - out: 0
 snap: 56 snapper 3 in: 0 - st: 0 - out: 0
 snap: 56 snapper 4 in: 0 - st: 1 - out: 0
 snap: 56 snapper 5 in: 0 - st: 1 - out: 0
 snap: 56 snapper 6 in: 0 - st: 1 - out: 0
 snap: 56 snapper 7 in: 0 - st: 0 - out: 0
 snap: 56 snapper 8 in: 0 - st: 0 - out: 0
 snap: 56 snapper 9 in: 0 - st: 0 - out: 0
 snap: 56 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 57 snapper 1 in: 1 - st: 1 - out: 1
 snap: 57 snapper 2 in: 1 - st: 0 - out: 0
 snap: 57 snapper 3 in: 0 - st: 0 - out: 0
 snap: 57 snapper 4 in: 0 - st: 1 - out: 0
 snap: 57 snapper 5 in: 0 - st: 1 - out: 0
 snap: 57 snapper 6 in: 0 - st: 1 - out: 0
 snap: 57 snapper 7 in: 0 - st: 0 - out: 0
 snap: 57 snapper 8 in: 0 - st: 0 - out: 0
 snap: 57 snapper 9 in: 0 - st: 0 - out: 0
 snap: 57 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 58 snapper 1 in: 1 - st: 0 - out: 0
 snap: 58 snapper 2 in: 0 - st: 1 - out: 0
 snap: 58 snapper 3 in: 0 - st: 0 - out: 0
 snap: 58 snapper 4 in: 0 - st: 1 - out: 0
 snap: 58 snapper 5 in: 0 - st: 1 - out: 0
 snap: 58 snapper 6 in: 0 - st: 1 - out: 0
 snap: 58 snapper 7 in: 0 - st: 0 - out: 0
 snap: 58 snapper 8 in: 0 - st: 0 - out: 0
 snap: 58 snapper 9 in: 0 - st: 0 - out: 0
 snap: 58 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 59 snapper 1 in: 1 - st: 1 - out: 1
 snap: 59 snapper 2 in: 1 - st: 1 - out: 1
 snap: 59 snapper 3 in: 1 - st: 0 - out: 0
 snap: 59 snapper 4 in: 0 - st: 1 - out: 0
 snap: 59 snapper 5 in: 0 - st: 1 - out: 0
 snap: 59 snapper 6 in: 0 - st: 1 - out: 0
 snap: 59 snapper 7 in: 0 - st: 0 - out: 0
 snap: 59 snapper 8 in: 0 - st: 0 - out: 0
 snap: 59 snapper 9 in: 0 - st: 0 - out: 0
 snap: 59 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 60 snapper 1 in: 1 - st: 0 - out: 0
 snap: 60 snapper 2 in: 0 - st: 0 - out: 0
 snap: 60 snapper 3 in: 0 - st: 1 - out: 0
 snap: 60 snapper 4 in: 0 - st: 1 - out: 0
 snap: 60 snapper 5 in: 0 - st: 1 - out: 0
 snap: 60 snapper 6 in: 0 - st: 1 - out: 0
 snap: 60 snapper 7 in: 0 - st: 0 - out: 0
 snap: 60 snapper 8 in: 0 - st: 0 - out: 0
 snap: 60 snapper 9 in: 0 - st: 0 - out: 0
 snap: 60 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 61 snapper 1 in: 1 - st: 1 - out: 1
 snap: 61 snapper 2 in: 1 - st: 0 - out: 0
 snap: 61 snapper 3 in: 0 - st: 1 - out: 0
 snap: 61 snapper 4 in: 0 - st: 1 - out: 0
 snap: 61 snapper 5 in: 0 - st: 1 - out: 0
 snap: 61 snapper 6 in: 0 - st: 1 - out: 0
 snap: 61 snapper 7 in: 0 - st: 0 - out: 0
 snap: 61 snapper 8 in: 0 - st: 0 - out: 0
 snap: 61 snapper 9 in: 0 - st: 0 - out: 0
 snap: 61 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 62 snapper 1 in: 1 - st: 0 - out: 0
 snap: 62 snapper 2 in: 0 - st: 1 - out: 0
 snap: 62 snapper 3 in: 0 - st: 1 - out: 0
 snap: 62 snapper 4 in: 0 - st: 1 - out: 0
 snap: 62 snapper 5 in: 0 - st: 1 - out: 0
 snap: 62 snapper 6 in: 0 - st: 1 - out: 0
 snap: 62 snapper 7 in: 0 - st: 0 - out: 0
 snap: 62 snapper 8 in: 0 - st: 0 - out: 0
 snap: 62 snapper 9 in: 0 - st: 0 - out: 0
 snap: 62 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 63 snapper 1 in: 1 - st: 1 - out: 1
 snap: 63 snapper 2 in: 1 - st: 1 - out: 1
 snap: 63 snapper 3 in: 1 - st: 1 - out: 1
 snap: 63 snapper 4 in: 1 - st: 1 - out: 1
 snap: 63 snapper 5 in: 1 - st: 1 - out: 1
 snap: 63 snapper 6 in: 1 - st: 1 - out: 1
 snap: 63 snapper 7 in: 1 - st: 0 - out: 0
 snap: 63 snapper 8 in: 0 - st: 0 - out: 0
 snap: 63 snapper 9 in: 0 - st: 0 - out: 0
 snap: 63 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 64 snapper 1 in: 1 - st: 0 - out: 0
 snap: 64 snapper 2 in: 0 - st: 0 - out: 0
 snap: 64 snapper 3 in: 0 - st: 0 - out: 0
 snap: 64 snapper 4 in: 0 - st: 0 - out: 0
 snap: 64 snapper 5 in: 0 - st: 0 - out: 0
 snap: 64 snapper 6 in: 0 - st: 0 - out: 0
 snap: 64 snapper 7 in: 0 - st: 1 - out: 0
 snap: 64 snapper 8 in: 0 - st: 0 - out: 0
 snap: 64 snapper 9 in: 0 - st: 0 - out: 0
 snap: 64 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 65 snapper 1 in: 1 - st: 1 - out: 1
 snap: 65 snapper 2 in: 1 - st: 0 - out: 0
 snap: 65 snapper 3 in: 0 - st: 0 - out: 0
 snap: 65 snapper 4 in: 0 - st: 0 - out: 0
 snap: 65 snapper 5 in: 0 - st: 0 - out: 0
 snap: 65 snapper 6 in: 0 - st: 0 - out: 0
 snap: 65 snapper 7 in: 0 - st: 1 - out: 0
 snap: 65 snapper 8 in: 0 - st: 0 - out: 0
 snap: 65 snapper 9 in: 0 - st: 0 - out: 0
 snap: 65 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 66 snapper 1 in: 1 - st: 0 - out: 0
 snap: 66 snapper 2 in: 0 - st: 1 - out: 0
 snap: 66 snapper 3 in: 0 - st: 0 - out: 0
 snap: 66 snapper 4 in: 0 - st: 0 - out: 0
 snap: 66 snapper 5 in: 0 - st: 0 - out: 0
 snap: 66 snapper 6 in: 0 - st: 0 - out: 0
 snap: 66 snapper 7 in: 0 - st: 1 - out: 0
 snap: 66 snapper 8 in: 0 - st: 0 - out: 0
 snap: 66 snapper 9 in: 0 - st: 0 - out: 0
 snap: 66 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 67 snapper 1 in: 1 - st: 1 - out: 1
 snap: 67 snapper 2 in: 1 - st: 1 - out: 1
 snap: 67 snapper 3 in: 1 - st: 0 - out: 0
 snap: 67 snapper 4 in: 0 - st: 0 - out: 0
 snap: 67 snapper 5 in: 0 - st: 0 - out: 0
 snap: 67 snapper 6 in: 0 - st: 0 - out: 0
 snap: 67 snapper 7 in: 0 - st: 1 - out: 0
 snap: 67 snapper 8 in: 0 - st: 0 - out: 0
 snap: 67 snapper 9 in: 0 - st: 0 - out: 0
 snap: 67 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 68 snapper 1 in: 1 - st: 0 - out: 0
 snap: 68 snapper 2 in: 0 - st: 0 - out: 0
 snap: 68 snapper 3 in: 0 - st: 1 - out: 0
 snap: 68 snapper 4 in: 0 - st: 0 - out: 0
 snap: 68 snapper 5 in: 0 - st: 0 - out: 0
 snap: 68 snapper 6 in: 0 - st: 0 - out: 0
 snap: 68 snapper 7 in: 0 - st: 1 - out: 0
 snap: 68 snapper 8 in: 0 - st: 0 - out: 0
 snap: 68 snapper 9 in: 0 - st: 0 - out: 0
 snap: 68 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 69 snapper 1 in: 1 - st: 1 - out: 1
 snap: 69 snapper 2 in: 1 - st: 0 - out: 0
 snap: 69 snapper 3 in: 0 - st: 1 - out: 0
 snap: 69 snapper 4 in: 0 - st: 0 - out: 0
 snap: 69 snapper 5 in: 0 - st: 0 - out: 0
 snap: 69 snapper 6 in: 0 - st: 0 - out: 0
 snap: 69 snapper 7 in: 0 - st: 1 - out: 0
 snap: 69 snapper 8 in: 0 - st: 0 - out: 0
 snap: 69 snapper 9 in: 0 - st: 0 - out: 0
 snap: 69 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 70 snapper 1 in: 1 - st: 0 - out: 0
 snap: 70 snapper 2 in: 0 - st: 1 - out: 0
 snap: 70 snapper 3 in: 0 - st: 1 - out: 0
 snap: 70 snapper 4 in: 0 - st: 0 - out: 0
 snap: 70 snapper 5 in: 0 - st: 0 - out: 0
 snap: 70 snapper 6 in: 0 - st: 0 - out: 0
 snap: 70 snapper 7 in: 0 - st: 1 - out: 0
 snap: 70 snapper 8 in: 0 - st: 0 - out: 0
 snap: 70 snapper 9 in: 0 - st: 0 - out: 0
 snap: 70 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 71 snapper 1 in: 1 - st: 1 - out: 1
 snap: 71 snapper 2 in: 1 - st: 1 - out: 1
 snap: 71 snapper 3 in: 1 - st: 1 - out: 1
 snap: 71 snapper 4 in: 1 - st: 0 - out: 0
 snap: 71 snapper 5 in: 0 - st: 0 - out: 0
 snap: 71 snapper 6 in: 0 - st: 0 - out: 0
 snap: 71 snapper 7 in: 0 - st: 1 - out: 0
 snap: 71 snapper 8 in: 0 - st: 0 - out: 0
 snap: 71 snapper 9 in: 0 - st: 0 - out: 0
 snap: 71 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 72 snapper 1 in: 1 - st: 0 - out: 0
 snap: 72 snapper 2 in: 0 - st: 0 - out: 0
 snap: 72 snapper 3 in: 0 - st: 0 - out: 0
 snap: 72 snapper 4 in: 0 - st: 1 - out: 0
 snap: 72 snapper 5 in: 0 - st: 0 - out: 0
 snap: 72 snapper 6 in: 0 - st: 0 - out: 0
 snap: 72 snapper 7 in: 0 - st: 1 - out: 0
 snap: 72 snapper 8 in: 0 - st: 0 - out: 0
 snap: 72 snapper 9 in: 0 - st: 0 - out: 0
 snap: 72 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 73 snapper 1 in: 1 - st: 1 - out: 1
 snap: 73 snapper 2 in: 1 - st: 0 - out: 0
 snap: 73 snapper 3 in: 0 - st: 0 - out: 0
 snap: 73 snapper 4 in: 0 - st: 1 - out: 0
 snap: 73 snapper 5 in: 0 - st: 0 - out: 0
 snap: 73 snapper 6 in: 0 - st: 0 - out: 0
 snap: 73 snapper 7 in: 0 - st: 1 - out: 0
 snap: 73 snapper 8 in: 0 - st: 0 - out: 0
 snap: 73 snapper 9 in: 0 - st: 0 - out: 0
 snap: 73 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 74 snapper 1 in: 1 - st: 0 - out: 0
 snap: 74 snapper 2 in: 0 - st: 1 - out: 0
 snap: 74 snapper 3 in: 0 - st: 0 - out: 0
 snap: 74 snapper 4 in: 0 - st: 1 - out: 0
 snap: 74 snapper 5 in: 0 - st: 0 - out: 0
 snap: 74 snapper 6 in: 0 - st: 0 - out: 0
 snap: 74 snapper 7 in: 0 - st: 1 - out: 0
 snap: 74 snapper 8 in: 0 - st: 0 - out: 0
 snap: 74 snapper 9 in: 0 - st: 0 - out: 0
 snap: 74 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 75 snapper 1 in: 1 - st: 1 - out: 1
 snap: 75 snapper 2 in: 1 - st: 1 - out: 1
 snap: 75 snapper 3 in: 1 - st: 0 - out: 0
 snap: 75 snapper 4 in: 0 - st: 1 - out: 0
 snap: 75 snapper 5 in: 0 - st: 0 - out: 0
 snap: 75 snapper 6 in: 0 - st: 0 - out: 0
 snap: 75 snapper 7 in: 0 - st: 1 - out: 0
 snap: 75 snapper 8 in: 0 - st: 0 - out: 0
 snap: 75 snapper 9 in: 0 - st: 0 - out: 0
 snap: 75 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 76 snapper 1 in: 1 - st: 0 - out: 0
 snap: 76 snapper 2 in: 0 - st: 0 - out: 0
 snap: 76 snapper 3 in: 0 - st: 1 - out: 0
 snap: 76 snapper 4 in: 0 - st: 1 - out: 0
 snap: 76 snapper 5 in: 0 - st: 0 - out: 0
 snap: 76 snapper 6 in: 0 - st: 0 - out: 0
 snap: 76 snapper 7 in: 0 - st: 1 - out: 0
 snap: 76 snapper 8 in: 0 - st: 0 - out: 0
 snap: 76 snapper 9 in: 0 - st: 0 - out: 0
 snap: 76 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 77 snapper 1 in: 1 - st: 1 - out: 1
 snap: 77 snapper 2 in: 1 - st: 0 - out: 0
 snap: 77 snapper 3 in: 0 - st: 1 - out: 0
 snap: 77 snapper 4 in: 0 - st: 1 - out: 0
 snap: 77 snapper 5 in: 0 - st: 0 - out: 0
 snap: 77 snapper 6 in: 0 - st: 0 - out: 0
 snap: 77 snapper 7 in: 0 - st: 1 - out: 0
 snap: 77 snapper 8 in: 0 - st: 0 - out: 0
 snap: 77 snapper 9 in: 0 - st: 0 - out: 0
 snap: 77 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 78 snapper 1 in: 1 - st: 0 - out: 0
 snap: 78 snapper 2 in: 0 - st: 1 - out: 0
 snap: 78 snapper 3 in: 0 - st: 1 - out: 0
 snap: 78 snapper 4 in: 0 - st: 1 - out: 0
 snap: 78 snapper 5 in: 0 - st: 0 - out: 0
 snap: 78 snapper 6 in: 0 - st: 0 - out: 0
 snap: 78 snapper 7 in: 0 - st: 1 - out: 0
 snap: 78 snapper 8 in: 0 - st: 0 - out: 0
 snap: 78 snapper 9 in: 0 - st: 0 - out: 0
 snap: 78 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 79 snapper 1 in: 1 - st: 1 - out: 1
 snap: 79 snapper 2 in: 1 - st: 1 - out: 1
 snap: 79 snapper 3 in: 1 - st: 1 - out: 1
 snap: 79 snapper 4 in: 1 - st: 1 - out: 1
 snap: 79 snapper 5 in: 1 - st: 0 - out: 0
 snap: 79 snapper 6 in: 0 - st: 0 - out: 0
 snap: 79 snapper 7 in: 0 - st: 1 - out: 0
 snap: 79 snapper 8 in: 0 - st: 0 - out: 0
 snap: 79 snapper 9 in: 0 - st: 0 - out: 0
 snap: 79 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 80 snapper 1 in: 1 - st: 0 - out: 0
 snap: 80 snapper 2 in: 0 - st: 0 - out: 0
 snap: 80 snapper 3 in: 0 - st: 0 - out: 0
 snap: 80 snapper 4 in: 0 - st: 0 - out: 0
 snap: 80 snapper 5 in: 0 - st: 1 - out: 0
 snap: 80 snapper 6 in: 0 - st: 0 - out: 0
 snap: 80 snapper 7 in: 0 - st: 1 - out: 0
 snap: 80 snapper 8 in: 0 - st: 0 - out: 0
 snap: 80 snapper 9 in: 0 - st: 0 - out: 0
 snap: 80 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 81 snapper 1 in: 1 - st: 1 - out: 1
 snap: 81 snapper 2 in: 1 - st: 0 - out: 0
 snap: 81 snapper 3 in: 0 - st: 0 - out: 0
 snap: 81 snapper 4 in: 0 - st: 0 - out: 0
 snap: 81 snapper 5 in: 0 - st: 1 - out: 0
 snap: 81 snapper 6 in: 0 - st: 0 - out: 0
 snap: 81 snapper 7 in: 0 - st: 1 - out: 0
 snap: 81 snapper 8 in: 0 - st: 0 - out: 0
 snap: 81 snapper 9 in: 0 - st: 0 - out: 0
 snap: 81 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 82 snapper 1 in: 1 - st: 0 - out: 0
 snap: 82 snapper 2 in: 0 - st: 1 - out: 0
 snap: 82 snapper 3 in: 0 - st: 0 - out: 0
 snap: 82 snapper 4 in: 0 - st: 0 - out: 0
 snap: 82 snapper 5 in: 0 - st: 1 - out: 0
 snap: 82 snapper 6 in: 0 - st: 0 - out: 0
 snap: 82 snapper 7 in: 0 - st: 1 - out: 0
 snap: 82 snapper 8 in: 0 - st: 0 - out: 0
 snap: 82 snapper 9 in: 0 - st: 0 - out: 0
 snap: 82 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 83 snapper 1 in: 1 - st: 1 - out: 1
 snap: 83 snapper 2 in: 1 - st: 1 - out: 1
 snap: 83 snapper 3 in: 1 - st: 0 - out: 0
 snap: 83 snapper 4 in: 0 - st: 0 - out: 0
 snap: 83 snapper 5 in: 0 - st: 1 - out: 0
 snap: 83 snapper 6 in: 0 - st: 0 - out: 0
 snap: 83 snapper 7 in: 0 - st: 1 - out: 0
 snap: 83 snapper 8 in: 0 - st: 0 - out: 0
 snap: 83 snapper 9 in: 0 - st: 0 - out: 0
 snap: 83 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 84 snapper 1 in: 1 - st: 0 - out: 0
 snap: 84 snapper 2 in: 0 - st: 0 - out: 0
 snap: 84 snapper 3 in: 0 - st: 1 - out: 0
 snap: 84 snapper 4 in: 0 - st: 0 - out: 0
 snap: 84 snapper 5 in: 0 - st: 1 - out: 0
 snap: 84 snapper 6 in: 0 - st: 0 - out: 0
 snap: 84 snapper 7 in: 0 - st: 1 - out: 0
 snap: 84 snapper 8 in: 0 - st: 0 - out: 0
 snap: 84 snapper 9 in: 0 - st: 0 - out: 0
 snap: 84 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 85 snapper 1 in: 1 - st: 1 - out: 1
 snap: 85 snapper 2 in: 1 - st: 0 - out: 0
 snap: 85 snapper 3 in: 0 - st: 1 - out: 0
 snap: 85 snapper 4 in: 0 - st: 0 - out: 0
 snap: 85 snapper 5 in: 0 - st: 1 - out: 0
 snap: 85 snapper 6 in: 0 - st: 0 - out: 0
 snap: 85 snapper 7 in: 0 - st: 1 - out: 0
 snap: 85 snapper 8 in: 0 - st: 0 - out: 0
 snap: 85 snapper 9 in: 0 - st: 0 - out: 0
 snap: 85 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 86 snapper 1 in: 1 - st: 0 - out: 0
 snap: 86 snapper 2 in: 0 - st: 1 - out: 0
 snap: 86 snapper 3 in: 0 - st: 1 - out: 0
 snap: 86 snapper 4 in: 0 - st: 0 - out: 0
 snap: 86 snapper 5 in: 0 - st: 1 - out: 0
 snap: 86 snapper 6 in: 0 - st: 0 - out: 0
 snap: 86 snapper 7 in: 0 - st: 1 - out: 0
 snap: 86 snapper 8 in: 0 - st: 0 - out: 0
 snap: 86 snapper 9 in: 0 - st: 0 - out: 0
 snap: 86 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 87 snapper 1 in: 1 - st: 1 - out: 1
 snap: 87 snapper 2 in: 1 - st: 1 - out: 1
 snap: 87 snapper 3 in: 1 - st: 1 - out: 1
 snap: 87 snapper 4 in: 1 - st: 0 - out: 0
 snap: 87 snapper 5 in: 0 - st: 1 - out: 0
 snap: 87 snapper 6 in: 0 - st: 0 - out: 0
 snap: 87 snapper 7 in: 0 - st: 1 - out: 0
 snap: 87 snapper 8 in: 0 - st: 0 - out: 0
 snap: 87 snapper 9 in: 0 - st: 0 - out: 0
 snap: 87 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 88 snapper 1 in: 1 - st: 0 - out: 0
 snap: 88 snapper 2 in: 0 - st: 0 - out: 0
 snap: 88 snapper 3 in: 0 - st: 0 - out: 0
 snap: 88 snapper 4 in: 0 - st: 1 - out: 0
 snap: 88 snapper 5 in: 0 - st: 1 - out: 0
 snap: 88 snapper 6 in: 0 - st: 0 - out: 0
 snap: 88 snapper 7 in: 0 - st: 1 - out: 0
 snap: 88 snapper 8 in: 0 - st: 0 - out: 0
 snap: 88 snapper 9 in: 0 - st: 0 - out: 0
 snap: 88 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 89 snapper 1 in: 1 - st: 1 - out: 1
 snap: 89 snapper 2 in: 1 - st: 0 - out: 0
 snap: 89 snapper 3 in: 0 - st: 0 - out: 0
 snap: 89 snapper 4 in: 0 - st: 1 - out: 0
 snap: 89 snapper 5 in: 0 - st: 1 - out: 0
 snap: 89 snapper 6 in: 0 - st: 0 - out: 0
 snap: 89 snapper 7 in: 0 - st: 1 - out: 0
 snap: 89 snapper 8 in: 0 - st: 0 - out: 0
 snap: 89 snapper 9 in: 0 - st: 0 - out: 0
 snap: 89 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 90 snapper 1 in: 1 - st: 0 - out: 0
 snap: 90 snapper 2 in: 0 - st: 1 - out: 0
 snap: 90 snapper 3 in: 0 - st: 0 - out: 0
 snap: 90 snapper 4 in: 0 - st: 1 - out: 0
 snap: 90 snapper 5 in: 0 - st: 1 - out: 0
 snap: 90 snapper 6 in: 0 - st: 0 - out: 0
 snap: 90 snapper 7 in: 0 - st: 1 - out: 0
 snap: 90 snapper 8 in: 0 - st: 0 - out: 0
 snap: 90 snapper 9 in: 0 - st: 0 - out: 0
 snap: 90 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 91 snapper 1 in: 1 - st: 1 - out: 1
 snap: 91 snapper 2 in: 1 - st: 1 - out: 1
 snap: 91 snapper 3 in: 1 - st: 0 - out: 0
 snap: 91 snapper 4 in: 0 - st: 1 - out: 0
 snap: 91 snapper 5 in: 0 - st: 1 - out: 0
 snap: 91 snapper 6 in: 0 - st: 0 - out: 0
 snap: 91 snapper 7 in: 0 - st: 1 - out: 0
 snap: 91 snapper 8 in: 0 - st: 0 - out: 0
 snap: 91 snapper 9 in: 0 - st: 0 - out: 0
 snap: 91 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 92 snapper 1 in: 1 - st: 0 - out: 0
 snap: 92 snapper 2 in: 0 - st: 0 - out: 0
 snap: 92 snapper 3 in: 0 - st: 1 - out: 0
 snap: 92 snapper 4 in: 0 - st: 1 - out: 0
 snap: 92 snapper 5 in: 0 - st: 1 - out: 0
 snap: 92 snapper 6 in: 0 - st: 0 - out: 0
 snap: 92 snapper 7 in: 0 - st: 1 - out: 0
 snap: 92 snapper 8 in: 0 - st: 0 - out: 0
 snap: 92 snapper 9 in: 0 - st: 0 - out: 0
 snap: 92 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 93 snapper 1 in: 1 - st: 1 - out: 1
 snap: 93 snapper 2 in: 1 - st: 0 - out: 0
 snap: 93 snapper 3 in: 0 - st: 1 - out: 0
 snap: 93 snapper 4 in: 0 - st: 1 - out: 0
 snap: 93 snapper 5 in: 0 - st: 1 - out: 0
 snap: 93 snapper 6 in: 0 - st: 0 - out: 0
 snap: 93 snapper 7 in: 0 - st: 1 - out: 0
 snap: 93 snapper 8 in: 0 - st: 0 - out: 0
 snap: 93 snapper 9 in: 0 - st: 0 - out: 0
 snap: 93 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 94 snapper 1 in: 1 - st: 0 - out: 0
 snap: 94 snapper 2 in: 0 - st: 1 - out: 0
 snap: 94 snapper 3 in: 0 - st: 1 - out: 0
 snap: 94 snapper 4 in: 0 - st: 1 - out: 0
 snap: 94 snapper 5 in: 0 - st: 1 - out: 0
 snap: 94 snapper 6 in: 0 - st: 0 - out: 0
 snap: 94 snapper 7 in: 0 - st: 1 - out: 0
 snap: 94 snapper 8 in: 0 - st: 0 - out: 0
 snap: 94 snapper 9 in: 0 - st: 0 - out: 0
 snap: 94 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 95 snapper 1 in: 1 - st: 1 - out: 1
 snap: 95 snapper 2 in: 1 - st: 1 - out: 1
 snap: 95 snapper 3 in: 1 - st: 1 - out: 1
 snap: 95 snapper 4 in: 1 - st: 1 - out: 1
 snap: 95 snapper 5 in: 1 - st: 1 - out: 1
 snap: 95 snapper 6 in: 1 - st: 0 - out: 0
 snap: 95 snapper 7 in: 0 - st: 1 - out: 0
 snap: 95 snapper 8 in: 0 - st: 0 - out: 0
 snap: 95 snapper 9 in: 0 - st: 0 - out: 0
 snap: 95 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 96 snapper 1 in: 1 - st: 0 - out: 0
 snap: 96 snapper 2 in: 0 - st: 0 - out: 0
 snap: 96 snapper 3 in: 0 - st: 0 - out: 0
 snap: 96 snapper 4 in: 0 - st: 0 - out: 0
 snap: 96 snapper 5 in: 0 - st: 0 - out: 0
 snap: 96 snapper 6 in: 0 - st: 1 - out: 0
 snap: 96 snapper 7 in: 0 - st: 1 - out: 0
 snap: 96 snapper 8 in: 0 - st: 0 - out: 0
 snap: 96 snapper 9 in: 0 - st: 0 - out: 0
 snap: 96 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 97 snapper 1 in: 1 - st: 1 - out: 1
 snap: 97 snapper 2 in: 1 - st: 0 - out: 0
 snap: 97 snapper 3 in: 0 - st: 0 - out: 0
 snap: 97 snapper 4 in: 0 - st: 0 - out: 0
 snap: 97 snapper 5 in: 0 - st: 0 - out: 0
 snap: 97 snapper 6 in: 0 - st: 1 - out: 0
 snap: 97 snapper 7 in: 0 - st: 1 - out: 0
 snap: 97 snapper 8 in: 0 - st: 0 - out: 0
 snap: 97 snapper 9 in: 0 - st: 0 - out: 0
 snap: 97 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 98 snapper 1 in: 1 - st: 0 - out: 0
 snap: 98 snapper 2 in: 0 - st: 1 - out: 0
 snap: 98 snapper 3 in: 0 - st: 0 - out: 0
 snap: 98 snapper 4 in: 0 - st: 0 - out: 0
 snap: 98 snapper 5 in: 0 - st: 0 - out: 0
 snap: 98 snapper 6 in: 0 - st: 1 - out: 0
 snap: 98 snapper 7 in: 0 - st: 1 - out: 0
 snap: 98 snapper 8 in: 0 - st: 0 - out: 0
 snap: 98 snapper 9 in: 0 - st: 0 - out: 0
 snap: 98 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 99 snapper 1 in: 1 - st: 1 - out: 1
 snap: 99 snapper 2 in: 1 - st: 1 - out: 1
 snap: 99 snapper 3 in: 1 - st: 0 - out: 0
 snap: 99 snapper 4 in: 0 - st: 0 - out: 0
 snap: 99 snapper 5 in: 0 - st: 0 - out: 0
 snap: 99 snapper 6 in: 0 - st: 1 - out: 0
 snap: 99 snapper 7 in: 0 - st: 1 - out: 0
 snap: 99 snapper 8 in: 0 - st: 0 - out: 0
 snap: 99 snapper 9 in: 0 - st: 0 - out: 0
 snap: 99 snapper 10 in: 0 - st: 0 - out: 0
 
 snap: 100 snapper 1 in: 1 - st: 0 - out: 0
 snap: 100 snapper 2 in: 0 - st: 0 - out: 0
 snap: 100 snapper 3 in: 0 - st: 1 - out: 0
 snap: 100 snapper 4 in: 0 - st: 0 - out: 0
 snap: 100 snapper 5 in: 0 - st: 0 - out: 0
 snap: 100 snapper 6 in: 0 - st: 1 - out: 0
 snap: 100 snapper 7 in: 0 - st: 1 - out: 0
 snap: 100 snapper 8 in: 0 - st: 0 - out: 0
 snap: 100 snapper 9 in: 0 - st: 0 - out: 0
 snap: 100 snapper 10 in: 0 - st: 0 - out: 0
 
 Case #1: OFF
 Program ended with exit code: 0*/