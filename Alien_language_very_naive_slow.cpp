//https://code.google.com/codejam/contest/90101/dashboard#s=p0&a=0
//I kept this to have an example of map permutation
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
#include <tr1/unordered_map>
using namespace std;

vector<string> get_permutation(vector<vector<string > > &allpos) {
    vector<string> tmp;
    vector<string> res;
    vector<vector<string > > allpostmp;
    if(allpos.size() != 2) {
        tmp = allpos[0];
        allpos.erase(allpos.begin());
        res = get_permutation(allpos);
        allpostmp.push_back(tmp);
        allpostmp.push_back(res);
    } else {
        allpostmp = allpos;
    }
    res.clear();
    auto len1 = allpostmp[0].size();
    auto len2 = allpostmp[1].size();
    for(int i=0; i < len1; ++i) {
        for(int k=0; k<len2; ++k) {
            string str;
            str = allpostmp[0][i]+allpostmp[1][k];
            res.push_back(str);
        }
    }
    return res;
}

vector<string> get_possiblities(const string & str, int L) {
    vector<string> plist;
    vector<vector<string > > allpos (L, std::vector<string>());
    int charpos = -1;
    bool inbrackets = false;
    bool previous_close = false;
    for(const auto &s: str) {
        if(s=='(') {
            if(!previous_close)
                charpos ++;
            inbrackets = true;
            previous_close = false;
        } else if(s==')') {
            inbrackets = false;
            charpos ++;
            previous_close = true;
        } else {
            if (charpos == -1) charpos ++;
            string tmp="";
            tmp += s;
            allpos[charpos].push_back(tmp);
            if(!inbrackets) charpos++;
            previous_close = false;
        }
    }
    plist = get_permutation(allpos);
    
    return plist;
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    //    tr1::unordered_map<string, bool> dictmap;
    map<string, bool> dictmap;
    
    unsigned int N, D, L;
    fin >> L >> D >> N;
    for(int d=0; d<D; ++d) {
        string str;
        fin>>str;
        dictmap[str] = true;
    }
    
    for (int c=1; c<=N; ++c) {
        int count = 0;
        string str;
        fin >> str;
        vector<string> plist = get_possiblities(str, L);
        for(const auto &l :plist) {
            if(dictmap[l]) count ++;
        }
        fout << "Case #" << c << ": " << count << endl;
        cout << "Case #" << c << ": " << count << endl;
    }
    fin.close();
    fout.close();
}

