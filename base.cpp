#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;


int main() {
    fstream fin;
    fstream fout;
    fout.open("output.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    int T;
    unsigned int N;
    fin >> T;
    
    for (int c=0; c<T; ++c) {
        fout << "Case #" << c+1 << ": " << endl;
        cout << "Case #" << c+1 << ": " << endl;
    }
    fin.close();
    fout.close();
}

