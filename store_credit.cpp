//https://code.google.com/codejam/contest/351101/dashboard#s=p0
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
using namespace std;
int main(){
    int T,C,I;
    vector<int> vec;
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    fin>>T;
    for(int i=0; i<T; ++i) {
        vec.clear();
        fin>>C;
        fin>>I;
        while(I--) {
            int p;
            fin>>p;
            vec.push_back(p);
        }
        for (int j=0; j<vec.size(); ++j) {
            if(i==26){
                cout << "case27" << endl;
            }
            int remain = C-vec[j];
            if(remain<=0) continue;
            auto f = find(vec.begin()+j+1, vec.end(), remain);
            if(f!=vec.end()) {
                long gok = (f-vec.begin()+1);
                cout << "Case #" << i+1 << ": " << j+1 << " " << f-vec.begin()+1 << " - " << vec.size()<<" / "<<C << " " << vec[j] << " " << *f << " - " << vec.at(j) << " "<< vec.at(gok-1) <<endl;
                
                fout << "Case #" << i+1 << ": " << j+1 << " " << f-vec.begin()+1<<endl;
                break;
            }
        }
    }
    fin.close();
    fout.close();
}