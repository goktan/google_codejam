//https://code.google.com/codejam/contest/90101/dashboard#s=p1&a=1
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;

struct Cell{
    int val;
    int H;
    int W;
    char basin;
    vector<Cell*> gets_from;
    vector<Cell*> sink_to;
    Cell(int v):val(v){basin='\0';};
};

bool comperator(Cell *a, Cell *b) {
    return a->val < b->val;
}

fstream fin;
fstream fout;

void print_cells(vector<vector<Cell> > wmap) {
    for(int i=0; i<wmap.size(); ++i) {
        for(int j=0; j<wmap[i].size(); ++j) {
            fout << wmap[i][j].basin << " ";
            cout << wmap[i][j].basin << " ";
        }
        fout << endl;
        cout << endl;
    }
}

char nextbasin = 'a';

void set_cell_basin(Cell *c) {
    if (c->basin == '\0') {
        for(auto it:c->sink_to) {
            if ((*it).basin != '\0') {
                c->basin = (*it).basin;
            }
        }
        for(auto it:c->gets_from) {
            if ((*it).basin != '\0') {
                c->basin = (*it).basin;
            }
        }
        if (c->basin == '\0') {
            c->basin = nextbasin;
            nextbasin ++;
        }
        for(auto it:c->sink_to) {
            set_cell_basin(&(*it));
        }
        for(auto it:c->gets_from) {
            set_cell_basin(&(*it));
        }
        
    }
}

int main() {
    
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    int T;
    unsigned int H, W;
    fin >> T;
    
    for (int t=0; t<T; ++t) {
        fin >> H >> W;
        vector<vector<Cell> > wmap(H);
        
        for(int i=0; i<H; ++i) {
            for(int j=0; j<W; ++j) {
                int tmp;
                fin >> tmp;
                wmap[i].push_back(tmp);
                wmap[i][j].H = i;
                wmap[i][j].W = j;
                
            }
        }
        
        for(int i=0; i<H; ++i) {
            for(int j=0; j<W; ++j) {
                if(i>0 && wmap[i][j].val < wmap[i-1][j].val) { //check up
                    wmap[i-1][j].sink_to.push_back(&wmap[i][j]);
                    wmap[i][j].gets_from.push_back(&wmap[i-1][j]);
                }
                if(j>0 && wmap[i][j].val < wmap[i][j-1].val) { //check left
                    wmap[i][j-1].sink_to.push_back(&wmap[i][j]);
                    wmap[i][j].gets_from.push_back(&wmap[i][j-1]);
                }
                if(j<W-1 && wmap[i][j].val < wmap[i][j+1].val) { //check right
                    wmap[i][j+1].sink_to.push_back(&wmap[i][j]);
                    wmap[i][j].gets_from.push_back(&wmap[i][j+1]);
                }
                if(i<H-1 && wmap[i][j].val < wmap[i+1][j].val) { //check down
                    wmap[i+1][j].sink_to.push_back(&wmap[i][j]);
                    wmap[i][j].gets_from.push_back(&wmap[i+1][j]);
                }
            }
        }
        
        for(int i=0; i<H; ++i) {
            for(int j=0; j<W; ++j) {
                if(wmap[i][j].sink_to.size() > 1) {
                    sort(wmap[i][j].sink_to.begin(), wmap[i][j].sink_to.end(),comperator);
                    auto it = wmap[i][j].sink_to.begin()+1;
                    while(it<wmap[i][j].sink_to.end()) {
                        (*it)->gets_from.erase(find((*it)->gets_from.begin(), (*it)->gets_from.end(), &wmap[i][j])); //delete cell from others gets_from
                        wmap[i][j].sink_to.erase(it);
                    }
                }
            }
        }
        nextbasin = 'a';
        for(int i=0; i<H; ++i) {
            for(int j=0; j<W; ++j) {
                set_cell_basin(&wmap[i][j]);
            }
        }
        
        
        fout << "Case #" << t+1 << ": " << endl;
        cout << "Case #" << t+1 << ": " << endl;
        print_cells(wmap);
    }
    fin.close();
    fout.close();
    return 1;
}

