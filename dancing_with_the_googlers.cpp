//https://code.google.com/codejam/contest/1460488/dashboard#s=p1
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;


int main() {
	fstream fin;
	fstream fout;
	fout.open("output.txt", ios::out);
	fin.open("input.txt", ios::in);
	if (!fout.is_open() || !fin.is_open()) {
		cerr << "file not opened" << endl;
		return EXIT_FAILURE;
	}

	int T, N, S, P;
	long int ti[100];
	fin >> T;
	for (int t = 0; t < T; ++t) {
		memset(ti, 0, sizeof(ti));
		fin >> N >> S >> P;
		for (int i = 0; i < N; ++i) {
			fin >> ti[i];
		}
		int hit = 0;
		int hit_suprise = 0;
		long int target = max((((P - 1) * 3) + 1), 0 );
		long int target_suprise = max((((P - 2) * 3) + 2), 0);
		for (int i = 0; i < N; ++i) {
			if (ti[i] == 0 && P!=0) continue;
			if (ti[i] >= target) hit++;
			else if (ti[i] >= target_suprise) hit_suprise++;
		}
		if (hit_suprise > S) hit_suprise = S;
		fout << "Case #" << t + 1 << ": " << hit+hit_suprise << endl;
		cout << "Case #" << t + 1 << ": " << hit+hit_suprise << endl;
	}

	fin.close();
	fout.close();
	getchar();
	return 0;
}

