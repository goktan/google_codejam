//https://code.google.com/codejam/contest/2994486/dashboard#s=p0&a=0
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <algorithm>
using namespace std;

class Node {
    char ch;
    unsigned int count;
public:
    Node(char c):ch(c){
        count = 1;
    }
    int get_count() const {
        return count;
    }
    void inc_count() {
        count++;
    }
    char get_ch() const {return ch;}
    
    static int compare(vector<Node> ndvec) {
        int total = 0;
        int res = 0;
        int minres = INT_MAX;
        int minnum = INT_MAX;
        int maxnum = INT_MIN;
        char basechar = '\0';
        for (const auto & nd : ndvec) {
            if(basechar == '\0') {
                basechar = nd.get_ch();
                minnum = min(minnum, nd.get_count());
                maxnum = max(maxnum, nd.get_count());
                total = nd.get_count();
            } else {
                if (basechar != nd.get_ch()) {
                    return -1;
                } else {
                    total += nd.get_count();
                    minnum = min(minnum, nd.get_count());
                    maxnum = max(maxnum, nd.get_count());
                }
            }
        }
        //int ave = round(total / ndvec.size());
        for (int k = minnum; k <= maxnum; ++k) {
            res = 0;
            for (const auto & nd : ndvec) {
                res += abs(nd.get_count()-k);
            }
            minres = min(minres, res);
        }
        return minres;
    }
};

class GTrie {
    vector<Node> ndvec;
    string str;
public:
    GTrie(string s) : str(s){
        create();
    }
    ~GTrie() {
        
    }
    void set_str(string s) {str = s;}
    void create() {
        for(char ch : str) {
            if (!ndvec.empty()) {
                auto ndlast = ndvec.end()-1;
                if (ndlast->get_ch() == ch) {
                    ndlast->inc_count();
                } else {
                    ndvec.push_back(ch);
                }
            } else {
                ndvec.push_back(ch);
            }
        }
    }
    vector<Node> get_ndvec() const {return ndvec;}
    string get_str() const {return str;}
    
    static int make_moves(vector<GTrie> gtvec) {
        vector<Node> ndvec;
        unsigned long int len = INT_MAX;
        for (const auto & gt : gtvec) {
            if(len == INT_MAX) {
                len = gt.get_ndvec().size();
            } else if (len != gt.get_ndvec().size()){
                return -1; //if the size of gtvecs dont match, we cannot build the string anyhow
            }
        }
        int total_move = 0;
        for (int i=0; i<len; ++i) {
            ndvec.clear();
            for (const auto & gt: gtvec) {
                ndvec.push_back(gt.get_ndvec().at(i));
            }
            int tmp = Node::compare(ndvec);
            if(tmp == -1) {
                return -1; // if chars at vector doesnt match, we cannot build the string
            } else {
                total_move += tmp;
            }
        }
        return total_move;
    }
};

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    int T, N;
    fin >> T;
    for (int c=0; c<T; ++c)
    {
        vector<GTrie> gtvec;
        string str;
        fin >> N;
        for(int i=0; i<N; ++i) {
            fin >> str;
            gtvec.push_back(str);
        }
        int moves = GTrie::make_moves(gtvec);
        if (moves == -1) {
            cout << "Case #" << c+1 << ": " << "Fegla Won" << endl;
            fout << "Case #" << c+1 << ": " << "Fegla Won" << endl;
        } else {
            cout << "Case #" << c+1 << ": " << moves << endl;
            fout << "Case #" << c+1 << ": " << moves << endl;
        }
    }
    fout.close();
}

