//https://code.google.com/codejam/contest/433101/dashboard#s=p2&a=0
//I also have a previous solution from 2010 but it is naive and can't calculate large set in time,
// so I did couple of optimizations, got rid of queue and changed it to array, kept a log of previous calculations
//more optimizations could be done like calculating the continous sequenceces, detecting them and adding sum but not necessary right now
//============================================================================
// Name        : coaster.cpp
// Author      : goktan
// Version     :
// Copyright   :
// Description : Hello World in C, Ansi-style
//============================================================================


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <map>
using namespace std;

typedef unsigned long int ulint;

unsigned long int main_loop(fstream &in) {
	ulint r, k;
    int n;
	in >> r >> k >> n;
    ulint qa[n];
    memset(qa, 0, sizeof(ulint) * n);
    pair<ulint, int> * hmap = new pair<ulint, int>{0, 0};
    
	for(int i = 0; i < n; ++i) {
		ulint tmp;
		in>>tmp;
        qa[i] = tmp;
        hmap[i] = {0, 0};
	}
	ulint roundg = n;
	ulint m = 0, t = 0;
    int last = 0, init = 0;
	for (ulint i=0; i < r ;++i) {

        bool f = false;
        while(1) {
			ulint a = qa[last];
            if(!f) {
                f = true;
                init = last;
                if(hmap[init].first != 0) {
                    t = hmap[last].first;
                    last = hmap[last].second;
                    break;
                }
            }
            if((t+a) > k || roundg == 0) {
                break;
            }
			t = t+a;
			last = (++last)%n;
			roundg --;
		}
        hmap[init].first = t;
        hmap[init].second = last;
		m += t;
		t = 0;
		roundg = n;
	}
    delete [] hmap;
	return m;
}

int main(void) {
	fstream in("input.txt", ios::in);
	fstream out("output.txt", ios::out | ios::trunc);
	if(!in.is_open() || !out.is_open()) {
		cerr << "unable to open files"<< endl;
		return -1;
	}
	int testcase = 0;
	in>>testcase;
	for(int j = 0; j < testcase; ++j) {
        auto count = main_loop(in);
        cout << "Case #" << j+1 << ": " << count << endl;
        out << "Case #" << j+1 << ": " << count << endl;
	}
	in.close();
	out.close();
	return 0;
}
