//https://code.google.com/codejam/contest/2994486/dashboard#s=p1&a=0
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <algorithm>
using namespace std;

typedef unsigned long long int ullint;

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    int T;
    ullint A, B, K;
    fin >> T;
    for (int c=0; c<T; ++c) {
        fin >> A >> B >> K;
        ullint count;
        if(A>K && B>K) count =(K*K)+((B-K)*K)+((A-K)*K);
        else if(A>K && B<K) count = (B*B)+((A-B)*B);
        else if(A<K && B>K) count = (A*A)+((B-A)*A);
        else count = A*B;
        //ullint count = 0;
        for (ullint a=K; a<A; ++a) {
            for (ullint b=K; b<B; ++b) {
                if ((a&b) < K) {
                    //cout << "hit-> "<<a << " " << b << endl;
                    count ++;
                } else {
                    //cout << "no hit-> " <<a << " " << b << endl;
                }
            }
        }
        cout << "Case #" << c+1 << ": " << count << endl;
        fout << "Case #" << c+1 << ": " << count << endl;
    }
    fout.close();
}

