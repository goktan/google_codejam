//https://code.google.com/codejam/contest/351101/dashboard#s=p1
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>

using namespace std;
int main(){
    int T;
    stack<string> st;
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    string s;
    fin>>T;
    getline(fin, s, '\n');
    for (int i =0 ; i<T; ++i){
        getline(fin, s, '\n');
        istringstream ss(s);
        char substr[1024];
        while(!ss.eof()) {
            ss >> substr;
            st.push(substr);
        }
        fout << "Case #" <<i+1 << ": ";
        while(!st.empty()) {
             fout << st.top()<< " ";
            st.pop();
        }
        fout << endl;
    }
    fin.close();
    fout.close();
}