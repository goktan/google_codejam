//
//  main.cpp
//  test3
//
//  naive solution from history!
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <cmath>

using std::cout;
using std::endl;
using std::stringstream;
using std::ifstream;
using std::ofstream;

class BOT;
int runOnce(std::string);
void readInputLine(std::string , BOT &, BOT &);

class BOT {
public:
    BOT();
    void add(int);
    unsigned int calculateNext();
    void moveNext();
    int moveSteps(int);
    void push();
    int getPosition();
private:
    std::vector<int> buttons;
    int position;
};

BOT::BOT() {
    position = 1;
}

void BOT::add(int place) 
{
    buttons.insert(buttons.begin(), place);
}
    
unsigned int BOT::calculateNext() 
{
    if (buttons.empty()) {
        return -1;
    }
    return abs(buttons.back()-position);
}

void BOT::moveNext() 
{
    position = buttons.back();
    buttons.pop_back();
    
}

int BOT::moveSteps(int step) 
{
    if(calculateNext() < step)
        step = calculateNext();
    if (buttons.back() > position) {
        position += step;
    } else {
        position -= step;
    }
    return calculateNext();
}

void BOT::push() 
{
    //do absolutely nothing
}

int BOT::getPosition() 
{
    return position;
}

__inline void readInputLine(std::string line, BOT &B, BOT &O, std::vector<char> &order) {
    char botName = 0, readed = 0;
    int buttonNum = 0, botButton = 0;
    
    char * tmpLine = strndup(line.c_str(), line.length()); 
    
    readed = sscanf(tmpLine, "%d", &buttonNum);
    //cout << buttonNum << endl;
    char * end = tmpLine + line.length();
    char * position = NULL;
    
    position = std::find(tmpLine, end, ' ');
    
    for (int i=0; i<buttonNum ; ++i) {
        readed = sscanf(position, " %c %d", &botName, &botButton);
//        cout <<"botname: " << botName << " " ;
        position = std::find(position+1, end, ' ');
        position = std::find(position+1, end, ' ');
        
     
//        cout << botButton << endl;
        if(botName == 'B') {
            B.add(botButton);
            order.insert(order.begin(), 'B');
            
        } else {
            O.add(botButton);
            order.insert(order.begin(), 'O');
        }
    }

    free(tmpLine); // not delete since we used strndup, a basic C kind of function
}

int runOnce(std::string line) 
{
    
    BOT B, O;
    std::vector<char> order;
    int moves = 0;
    readInputLine(line, B, O, order);
    
    while(order.size()!=0) {
        unsigned int bNext = B.calculateNext();
        unsigned int oNext = O.calculateNext();
        if(order.back() == 'B') {
            B.moveNext();
            B.push();
            if(oNext != -1)
                O.moveSteps(bNext + 1); // +1 is for pushing
            moves += bNext + 1;
            order.pop_back();
        } else if (order.back() == 'O') {
            O.moveNext();
            O.push();
            if(bNext != -1)
                B.moveSteps(oNext + 1);
            moves += oNext + 1;
            order.pop_back();
        }     }
    
    return moves;
}

int main (int argc, const char * argv[])
{
    
    cout << "Using file " << argv[1] << endl;
    ifstream in(argv[1], std::ifstream::in);
    ofstream fout(argv[2], std::ofstream::out);
    if(!in.is_open()) {
        std::cerr << "Unable to open file"<< endl;
        return EXIT_FAILURE;
    }
    int lineNum = 0;
    std::string line;
    in >> lineNum;
    getline(in, line);
    for (int i = 0; i < lineNum; ++i) {
        getline(in, line, '\n');
        cout << "Case #" << i+1 << ": " << runOnce(line) << endl;
        fout << "Case #" << i+1 << ": " << runOnce(line) << endl;
    }
    fout.close();
    return EXIT_SUCCESS;
}

