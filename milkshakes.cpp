//https://code.google.com/codejam/contest/32016/dashboard#s=p1&a=1
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <algorithm>
using namespace std;

bool is_satisfied(vector<vector<int> > &cust, vector<int> &flav) {
    bool res = false;
    for (const auto &cust_flavs: cust ) {
        for (int i=0; i<cust_flavs.size(); ++i) {
            if(cust_flavs[i] != flav[i] && cust_flavs[i] != -1) {
                res = false;
                break;
            } else res |= true;
        }
        if(res) break;
    }
    return res;
}

int get_malt_flav(vector<vector<int> > cust) {
    for (const auto &cust_flavs: cust ) {
        for (int i=0; i<cust_flavs.size(); ++i) {
            if(cust_flavs[i]==1) return i;
        }
    }
    return -1;
}

string tostring_flavor(const vector<int> flav) {
    ostringstream ss;
    for (int i=0; i<flav.size(); ++i) {
        ss << flav[i] << " " ;
    }
    return ss.str();
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }

    int C, N, M, L, X, Y;
    vector<vector<vector<int> > >  clikes;
    //first vector customers
    //second vector choices
    //third vector deep choices

    fin >> C;
    for (int c=0; c<C; ++c) {
        clikes.clear();
        fin >> N >> M;
        clikes.assign(M,0);
        for (int m = 0; m<M; ++m ) {
            fin >> L;
            for (int l=0; l<L; ++l ) {
                fin >> X >> Y;
                vector<int> tmp(N,-1);
                tmp[X-1] = Y;
                clikes[m].push_back(tmp);
            }
        }
        
        vector<int> flav(N, 0);
        bool possible = true;
        for(int i=0; i<clikes.size(); ++i) {
            if(!is_satisfied(clikes[i], flav)) {
                int pos = get_malt_flav(clikes[i]);
                if(pos == -1) {
                    //IMPOSSIBLE
                    fout << "Case #" << c+1 << ": " << "IMPOSSIBLE" << endl;
                    cout << "Case #" << c+1 << ": " << "IMPOSSIBLE" << endl;
                    possible = false;
                    break;
                }
                flav[pos] = 1;
                i = -1; // return to beginning
            }
        }
        if (possible) {
            fout << "Case #" << c+1 << ": " << tostring_flavor(flav) << endl;
            cout << "Case #" << c+1 << ": " << tostring_flavor(flav) << " " << N <<" " <<  M << endl;
        }

    }
    fin.close();
    fout.close();
}

