//https://code.google.com/codejam/contest/2270488/dashboard
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;


int main() {
	fstream fin;
	fstream fout;
	fout.open("output.txt", ios::out);
	fin.open("input.txt", ios::in);
	if (!fout.is_open() || !fin.is_open()) {
		cerr << "file not opened" << endl;
		return EXIT_FAILURE;
	}

	int T;
	fin >> T;
	int table[4][4];
	char c;
	for (int t = 0; t < T; ++t) {
		memset(table, 0, sizeof(table));
		for (int i = 0; i < 4; ++i) {
			for (int j = 0; j < 4; ++j) {
				fin >> c;
				if (c == 'X') table[i][j] = -1;
				else if (c == 'O') table[i][j] = 1;
				else if (c == 'T') table[i][j] = 10;
				else table[i][j] = 100;
			}
		}
		//fin >> c;
		//if the sum of any diagonal, row or column is equal to 4 or 13, O wins
		//if the sum of any diagonal, row or column is equal to -4 or 7, X wins
		//if the sum of any diagonal, row or column is not equal to above and bigger than 100, then not complete
		//if anything else than above, then Draw
		int res[10];
		memset(res, 0, sizeof(res));
		for (int i = 0; i < 4; ++i) {
			res[i] = table[i][0] + table[i][1] + table[i][2] + table[i][3];
			res[i+4] = table[0][i] + table[1][i] + table[2][i] + table[3][i];
		}
		res[8] = table[0][0] + table[1][1] + table[2][2] + table[3][3];
		res[9] = table[0][3] + table[1][2] + table[2][1] + table[3][0];
		bool winner = false;
		bool empty = false;
		for (int i = 0; i < 10; ++i) {
			if (res[i] == 4 || res[i] == 13) {
				fout << "Case #" << t + 1 << ": O won" << endl;
				cout << "Case #" << t + 1 << ": O won" << endl;
				winner = true;
				break;
			} 
			if (res[i] == -4 || res[i] == 7) {
				fout << "Case #" << t + 1 << ": X won" << endl;
				cout << "Case #" << t + 1 << ": X won" << endl;
				winner = true;
				break;
			}
			if (res[i] > 50) empty = true;
		}
		if (!winner) {
			if (empty) {
				fout << "Case #" << t + 1 << ": Game has not completed" << endl;
				cout << "Case #" << t + 1 << ": Game has not completed" << endl;
			}
			else {
				fout << "Case #" << t + 1 << ": Draw" << endl;
				cout << "Case #" << t + 1 << ": Draw" << endl;
			}
		}
	}

	fin.close();
	fout.close();
	getchar();
	return 0;
}

