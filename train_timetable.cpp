//https://code.google.com/codejam/contest/32013/dashboard#s=p1
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;

int day_min(char day[]) {
    int min_num=INT_MAX;
    for (int i = 0; i < 1440; ++i) {
        min_num = min(min_num, (int)day[i]);
    }
    return min_num;
}

void print_array(char day[]) {
    for (int i = 0; i < 1440; ++i) {
        if(i%60==0) cout << endl << i/60 << "-> " ;
        cout << (int)day[i] << " ";

     }
    cout << endl << endl;
}

int clock_to_day(int h, int m) {
    int retval = -1;
    retval = 60*h+m;
    if(retval>1440)
        retval = 1440;
    return retval;
}

void day_to_clock(int day, int &h, int &m) {
    h = (day%1440) / 60;
    m = day % 60;
}

void fill_day(char day[], int start, int end, int sum) {
    if (end> 1440) {
        end = 1440;
    }
    for (int i=start; i<end; ++i) {
        day[i] += sum;
    }
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    char ttA[1440], ttB[1440];
    memset(ttA, 0, sizeof(char)*1440);
    memset(ttB, 0, sizeof(char)*1440);
    int T;
    unsigned int N, NA, NB;
    fin >> N;
    
    for (int n=0; n<N; ++n) {
        memset(ttA, 0, sizeof(char)*1440);
        memset(ttB, 0, sizeof(char)*1440);
        fin >> T;
        fin >> NA >> NB;
        int start_h;
        int start_m;
        int finish_h;
        int finish_m;
        int start;
        int finish;
        char tmp;
        for(int na=0; na<NA; ++na) {
            fin >> start_h >> tmp >>start_m >> finish_h >> tmp >> finish_m;
            start = clock_to_day(start_h, start_m);
            finish = clock_to_day(finish_h, finish_m);
            
            fill_day(ttA, start, 1440, -1);
            fill_day(ttB, finish+T, 1440, 1);
            
        }
        for(int nb=0; nb<NB; ++nb) {
            fin >> start_h >> tmp >>start_m >> finish_h >> tmp >> finish_m;
            start = clock_to_day(start_h, start_m);
            finish = clock_to_day(finish_h, finish_m);
            fill_day(ttB, start, 1440, -1);
            fill_day(ttA, finish+T, 1440, 1);
            
        }
        //print_array(ttA);
        //print_array(ttB);
        
        
        fout << "Case #" << n+1 << ": " << abs(day_min(ttA)) << " " << abs(day_min(ttB)) << endl;
        cout << "Case #" << n+1 << ": " << abs(day_min(ttA)) << " " << abs(day_min(ttB)) << endl;
    }
    fin.close();
    fout.close();
}

