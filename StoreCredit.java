import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

public class StoreCredit
{ 
 public static void main(String[] args) throws Exception
 {
  Scanner scanner = new Scanner(new File("input.txt"));
  PrintWriter pw = new PrintWriter("A-large-practice.ou"); 
  int _case = 1;
  
  scanner.nextLine();
  while(scanner.hasNextLine())
  {
   int c = Integer.parseInt(scanner.nextLine()); //Amount of credit you have at the store
   int i = Integer.parseInt(scanner.nextLine()); // Number of items in the store
   String prices = scanner.nextLine();
  
   /* Available items */
   Item[] l = new Item[i];
  
   String[] ap = prices.split(" ");
   for(int x = 0; x < i; x++)  
    l[x] = new Item(Integer.parseInt(ap[x]));
  
  
   search:for(int x = 0; x < i-1; x++)
   {
    for(int j = x+1; j < i; j++)
    {
     if(l[x].price + l[j].price == c)
     {
      System.out.printf("Case #%d: %d %d\n", _case, x+1, j+1);
      pw.printf("Case #%d: %d %d\n", _case, x+1, j+1);
      break search;
     }
    }
   }
   _case++;
  }
  
  scanner.close();
  pw.close();
 }
}


class Item
{
 int price;
 
 Item(int price)
 {
  this.price = price;
 }
}