
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <algorithm>
using namespace std;

class City {
    int ZIP;
    int name;
    vector<City> flights;
    bool visited;
public:
    static int GLOBAL_NAME_LAST;
    
    City(int z) : ZIP(z) {
        GLOBAL_NAME_LAST++;
        name = GLOBAL_NAME_LAST;
        visited = false;
    }
    void add_flight(City&f) {
        flights.push_back(f);
    }
    int get_ZIP() const {
        return ZIP;
    }
    int get_name() const {
        return name;
    }

    void now_visited() {
        visited = true;
    }

    bool is_visited() const {return visited;}
    
    friend bool operator< (City const &i, City const &j);
};
bool operator< (City const &i, City const &j) { return (i.ZIP<j.ZIP);}

int City::GLOBAL_NAME_LAST = 0;
stack<City*> ctstack;


void calculate_route(City *source) {
    
    ctstack.push(source);
    //implement a dept-first-search algorithm here

}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }

    int T, N, M;
    vector<City> ctvec;

    fin >> T;
    for (int c=0; c<T; ++c) {
        City::GLOBAL_NAME_LAST = 0;
        ctvec.clear();
        fin >> N >> M;
        for(int i=0; i<N; ++i) {
            int tmp_zip;
            fin >> tmp_zip;
            ctvec.push_back(tmp_zip);
        }
        for (int i=0; i<M; ++i) {
            int from, to;
            fin >> from >> to;
            ctvec[from-1].add_flight(ctvec[to-1]);
            ctvec[to-1].add_flight(ctvec[from-1]);
        }
        sort(ctvec.begin(), ctvec.end());

        calculate_route(&ctvec[0]);
        cout << endl;
    }
    fin.close();
    fout.close();
}

