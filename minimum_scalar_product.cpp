//https://code.google.com/codejam/contest/32016/dashboard#s=p0
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <algorithm>
using namespace std;

void fill_vector(istream &fin, vector<long long int> &vec, int N) {
    vec.clear();
    for (int i=0; i<N; ++i) {
        int tmp;
        fin >> tmp;
        vec.push_back(tmp);
    }
}

int main() {
    fstream fin;
    fstream fout;
    fout.open("output1.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }

    int T, N;
    vector<long long int> vec1, vec2;

    fin >> T;
    for (int c=0; c<T; ++c) {
        long long   int scalar = 0;
        fin >> N;
        fill_vector(fin, vec1, N);
        fill_vector(fin, vec2, N);
        sort(vec1.begin(), vec1.end());
        sort(vec2.begin(), vec2.end());
        vector<long long int>::iterator it1_beg = vec1.begin();
        vector<long long int>::iterator it2_end = vec2.end()-1;
        while(it1_beg != vec1.end()) {
            scalar += (*it1_beg) * (*it2_end);
            it1_beg ++;
            it2_end --;
        }


        fout << "Case #" << c+1 << ": " << scalar << endl;
        cout << "Case #" << c+1 << ": " << scalar << endl;
    }
    fin.close();
    fout.close();
}

