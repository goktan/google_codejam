//============================================================================
// Name        : save_universe.cpp
// Author      : goktan
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <algorithm>
using namespace std;

int main_loop(fstream &in) {
	vector<string> engines;
	vector<string> queries;
	vector<string>::iterator qi;
	int switchcount = 0;
	int enginenum = 0;
	int querynum = 0;
	in>>enginenum;

	cout << "Engines..." << endl;
	for(int j= 0; j < enginenum; ++j) {
		//in.clear();
		char tmp[101];
		memset(tmp, 0, 101);
		in.getline(tmp, 100);
		//string tmp;
		//in >> tmp;
		engines.push_back(tmp);
		cout << tmp << endl;
	}
	in>>querynum;
	cout << "Queries..." << endl;

	for(int j=0; j < querynum; ++j) {
		if(!in.good()) {
			cerr << "not good" << endl;
		}
		//in.clear();
		char tmp[101];
		memset(tmp, 0, 101);
		in.getline(tmp, 100);
		//string tmp;
		//in >> tmp;
		queries.push_back(tmp);
		cout << tmp << endl;
	}

	qi = queries.begin();
	string bestengine;
	vector<string>::iterator bestpos = queries.begin();
	while(qi != queries.end()) {
		for (int i = 0; i < enginenum; ++i) {
			qi = find(qi, queries.end(), engines.at(i)); //!= vector.end();
			if(qi > bestpos || 	qi == queries.end()) {
				bestpos = qi;
				bestengine = engines.at(i);
			}
		}
		cout << bestengine << " " << ends;
		switchcount ++;
		qi = bestpos + 1 ;
	}
	cout << endl;
	return switchcount;
}

int main() {
	fstream in("input", ios::in);
	fstream out("output", ios::out | ios::trunc);
	if(!in.is_open() || !out.is_open()) {
		cerr << "unable to open files"<< endl;
		return -1;
	}
	int testcase = 0;
	in>>testcase;
	for(int j = 0; j < testcase; ++j) {
		int count = main_loop(in);
		out << "Case #" << j+1 << ": " << count << endl;

	}
	in.close();
	out.close();
	return 0;
}
