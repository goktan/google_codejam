//https://code.google.com/codejam/contest/1460488/dashboard#s=a
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <stack>
#include <sstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <stdint.h>
using namespace std;


int main() {
    fstream fin;
    fstream fout;
    fout.open("output.txt", ios::out);
    fin.open("input.txt", ios::in);
    if(!fout.is_open() || !fin.is_open()) {
        cerr << "file not opened" << endl;
        return EXIT_FAILURE;
    }
    
    map<char, char> dict;
    dict['a'] = 'y';
    dict['b'] = 'h';
    dict['c'] = 'e';
    dict['d'] = 's';
    dict['e'] = 'o';
    dict['f'] = 'c';
    dict['g'] = 'v';
    dict['h'] = 'x';
    dict['i'] = 'd';
    dict['j'] = 'u';
    dict['k'] = 'i';
    dict['l'] = 'g';
    dict['m'] = 'l';
    dict['n'] = 'b';
    dict['o'] = 'k';
    dict['p'] = 'r';
    dict['q'] = 'z';
    dict['r'] = 't';
    dict['s'] = 'n';
    dict['t'] = 'w';
    dict['u'] = 'j';
    dict['v'] = 'p';
    dict['w'] = 'f';
    dict['x'] = 'm';
    dict['y'] = 'a';
    dict['z'] = 'q';
    dict[' '] = ' ';

    
    int T;
    fin >> T;
    char buf[256];
    fin.getline(buf, 255);

    for (int c=0; c<T; ++c) {
        fout << "Case #" << c+1 << ": ";
        cout << "Case #" << c+1 << ": ";
        memset(buf, 0, sizeof(buf));
        fin.getline(buf, 255);
        
        for(int i=0; i<strlen(buf); ++i) {
            fout<<dict[buf[i]];
            cout<<dict[buf[i]];
        }
        
        fout << endl;
        cout << endl;

    }
    fin.close();
    fout.close();
}

