//
//  main.cpp
//  test3
//
//  Created by goktan kantarcioglu on 2/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip.h>

#include <cmath>

using std::cout;
using std::endl;
using std::stringstream;
using std::ifstream;
using std::ofstream;


int runOnce(std::string , int ); 

__inline void readInputLine(std::string line, std::vector <int> &array, int len) {
    int tmp;
    stringstream ss (stringstream::in | stringstream::out);
    ss << line;
    array.push_back(0);
    for (int i=0; i<len ; ++i) {
        ss>> tmp;
        array.push_back(tmp);
    }

}

int runOnce(std::string line, int arrayNum) 
{
    int moves = 0;
    std::vector <int> array;
    readInputLine(line, array, arrayNum);
    int start = 1;
    int tmp;
  /*  
    while(true) {
        int i;
        for(i=start; i <= arrayNum; ++i) {
            if(array[i] != i) {
                start = i;
                break;
            }
        }
        if (i == arrayNum) break;
        tmp = array[start];
        array[start] = array[tmp];
        array[tmp] = tmp;
        moves += 2;
    }
    */
    for (int i = 1; i <= arrayNum; ++i) {
        if(array[i] != i) moves ++;
    }
    return moves;
}

int main (int argc, const char * argv[])
{
    
    cout << "Using file " << argv[1] << endl;
    ifstream in(argv[1], std::ifstream::in);
    ofstream fout(argv[2], std::ofstream::out);
    if(!in.is_open()) {
        std::cerr << "Unable to open file"<< endl;
        return EXIT_FAILURE;
    }
    int lineNum = 0, arrayNum = 0;
    std::string line;
    in >> lineNum;
    getline(in, line);
    for (int i = 0; i < lineNum; ++i) {
        in >> arrayNum;
        getline(in, line, '\n');
        getline(in, line, '\n');
        double output = (double) runOnce(line, arrayNum);
        cout << "Case #" << i+1 << ": " << setiosflags(ios::fixed) << std::setprecision(6) << output << endl;
        fout << "Case #" << i+1 << ": " << setiosflags(ios::fixed) << std::setprecision(6) << output << endl;    }
    fout.close();
    return EXIT_SUCCESS;
}

